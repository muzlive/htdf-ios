//
//  Common.m
//  Cultwo
//
//  Created by ParkJongsung on 2016. 12. 11..
//  Copyright © 2016년 xSync. All rights reserved.
//

#import "Common.h"

@implementation Common

- (id) init{
    if (self = [super init]) {
        
    }
    return self;
}

-(void)setUserId:(NSString *)userId{
    _userId = userId;
    SET_VALUE(userId, @"userId");
}

+ (Common *)sharedInstance
{
    static dispatch_once_t once;
    static Common *__sharedInstance = nil;
    if (!__sharedInstance) {
        dispatch_once(&once, ^{
            __sharedInstance = [[[self class] alloc] init];
            __sharedInstance.userId = GET_VALUE(@"userId");
        });
    }
    return __sharedInstance;
}



@end
