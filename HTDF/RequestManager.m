//
//  SKKRequestManager.m
//  SK Knights
//
//  Created by 김기범 on 2016. 10. 17..
//  Copyright © 2016년 xSync. All rights reserved.
//

#import "RequestManager.h"
#import "Common.h"
#import "RatingPopUp.h"

#define kAPIHost @"http://211.249.62.55:8060"
@interface RequestManager()
@property AFHTTPSessionManager *sessionManager;
@property NSString *serverURL;
@end

#define CODE_SUCCESS 200


@implementation RequestManager

- (id)init {
    self = [super init];
    self.serverURL = kAPIHost;
    self.sessionManager = [AFHTTPSessionManager manager];
    return self;
}
//
#pragma mark - Login
- (void)getCurrentPerformanceDataWithCompletion:(void(^)(NSDictionary *))completion{

    [self.sessionManager GET:[NSString stringWithFormat:@"%@/api/currentPerformance", kAPIHost]
                  parameters:@{}
                     progress:nil
                      success:^(NSURLSessionDataTask * _Nonnull task, NSDictionary * _Nullable responseObject) {
                          completion(responseObject);
                      }
                     failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                          [[NSNotificationCenter defaultCenter] postNotificationName:@"connectError"
                                            object:nil];
                      }];
} 


-(void)checkUser:(NSString*)udid
       pushToken:(NSString *)pushToken
      completion:(void(^)(NSDictionary *))completion{
    [self.sessionManager GET:[NSString stringWithFormat:@"%@/api/checkUser", kAPIHost]
                  parameters:@{@"udid" : udid,
                               @"pushToken" : pushToken,
                               @"devicePlatform" : @"iOS"}
                    progress:nil
                     success:^(NSURLSessionDataTask * _Nonnull task, NSDictionary * _Nullable responseObject) {
                         completion(responseObject);
                         if( [[responseObject objectForKey:@"code"] intValue] == 200){
                             [self checkVovWithCompletion:^(NSDictionary *data) {
                                 [RatingPopUp presentPopUpTo:[Functions topMostController]
                                                 withVOVData:data
                                                  completion:^{
                                                      
                                                  }];
                             }];
                         }
                         
                     }
                     failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                         [[NSNotificationCenter defaultCenter] postNotificationName:@"connectError"
                                            object:nil];
                     }];
}


/// Map info as rects
- (void)getCompanyInfoListWithCompletion:(void(^)(NSArray *))completion {
    
    [self.sessionManager GET:[NSString stringWithFormat:@"%@/api/companies", kAPIHost]
                  parameters:@{}
                    progress:nil
                     success:^(NSURLSessionDataTask * _Nonnull task, NSDictionary * _Nullable responseObject) {
                         if ([[responseObject objectForKey:@"code"] intValue]==CODE_SUCCESS) {
                             NSArray *array = responseObject[@"object"];
                             completion(array);
                         } else {
                             // error
                             completion(nil);
                         }
                     }
                     failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                         [[NSNotificationCenter defaultCenter] postNotificationName:@"connectError"
                                                                             object:nil];
                     }];
}


- (void)getFavoriteListWithCompletion:(void(^)(NSArray *))completion {
    
    [self.sessionManager GET:[NSString stringWithFormat:@"%@/api/getFavorCompanies?user_id=%@",
                              kAPIHost, [Common sharedInstance].userId]
                  parameters:@{}
                    progress:nil
                     success:^(NSURLSessionDataTask * _Nonnull task, NSDictionary * _Nullable responseObject) {
                         if ([[responseObject objectForKey:@"code"] intValue]==CODE_SUCCESS) {
                             NSArray *array = responseObject[@"object"];
                             completion(array);
                         } else {
                             // error
                             completion(NO);
                         }
                     }
                     failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                         [[NSNotificationCenter defaultCenter] postNotificationName:@"connectError"
                                                                             object:nil];
                     }];
}
- (void)addToFavoriteListWithId:(int)companyId completion:(void(^)(BOOL))completion {
    
    [self.sessionManager GET:[NSString stringWithFormat:@"%@/api/favorCompany?user_id=%@&company_id=%d",
                              kAPIHost, [Common sharedInstance].userId, companyId]
                  parameters:@{}
                    progress:nil
                     success:^(NSURLSessionDataTask * _Nonnull task, NSDictionary * _Nullable responseObject) {
                         if ([[responseObject objectForKey:@"code"] intValue]==CODE_SUCCESS) {
                             completion(YES);
                         } else {
                             // error
                             completion(NO);
                         }
                     }
                     failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                         [[NSNotificationCenter defaultCenter] postNotificationName:@"connectError"
                                                                             object:nil];
                     }];
}
- (void)removeFromFavoriteListWithId:(int)companyId completion:(void(^)(BOOL))completion {
    
    [self.sessionManager GET:[NSString stringWithFormat:@"%@/api/disfavorCompany?user_id=%@&company_id=%d",
                              kAPIHost, [Common sharedInstance].userId, companyId]
                  parameters:@{}
                    progress:nil
                     success:^(NSURLSessionDataTask * _Nonnull task, NSDictionary * _Nullable responseObject) {
                         if ([[responseObject objectForKey:@"code"] intValue]==CODE_SUCCESS) {
                             completion(YES);
                         } else {
                             // error
                             completion(NO);
                         }
                     }
                     failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                         [[NSNotificationCenter defaultCenter] postNotificationName:@"connectError"
                                                                             object:nil];
                     }];
}

- (void)rateCompanyWithId:(int)companyId rating:(CGFloat)rate completion:(void(^)(BOOL))completion {
    
    [self.sessionManager GET:[NSString stringWithFormat:@"%@/api/rateCompany?user_id=%@&company_id=%d&rate=%f",
                              kAPIHost, [Common sharedInstance].userId, companyId, rate]
                  parameters:@{}
                    progress:nil
                     success:^(NSURLSessionDataTask * _Nonnull task, NSDictionary * _Nullable responseObject) {
                         if ([[responseObject objectForKey:@"code"] intValue]==CODE_SUCCESS) {
                             completion(YES);
                         } else {
                             // error
                             completion(NO);
                         }
                     }
                     failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                         [[NSNotificationCenter defaultCenter] postNotificationName:@"connectError"
                                                                             object:nil];
                     }];
}



- (void)rateVOVWithId:(int)vovId
               rating:(CGFloat)rate
           completion:(void(^)(BOOL))completion {
    
    [self.sessionManager GET:[NSString stringWithFormat:@"%@/api/rateVOV?user_id=%@&vov_id=%d&rate=%f",
                              kAPIHost, [Common sharedInstance].userId
                              , vovId, rate]
                  parameters:@{}
                    progress:nil
                     success:^(NSURLSessionDataTask * _Nonnull task, NSDictionary * _Nullable responseObject) {
                         if ([[responseObject objectForKey:@"code"] intValue]==CODE_SUCCESS) {
                             completion(YES);
                         } else {
                             // error
                             completion(NO);
                         }
                     }
                     failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                         [[NSNotificationCenter defaultCenter] postNotificationName:@"connectError"
                                                                             object:nil];
                     }];
}



- (void)checkVovWithCompletion:(void(^)(NSDictionary *))completion {
    
    [self.sessionManager GET:[NSString stringWithFormat:@"%@/api/getCurrentVOV?user_id=%@",
                              kAPIHost,
                              [Common sharedInstance].userId]
                  parameters:@{}
                    progress:nil
                     success:^(NSURLSessionDataTask * _Nonnull task, NSDictionary * _Nullable responseObject) {
                         if ([[responseObject objectForKey:@"code"] intValue]==CODE_SUCCESS) {
                             completion(responseObject[@"object"]);
                         }
                     }
                     failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                         [[NSNotificationCenter defaultCenter] postNotificationName:@"connectError"
                                                                             object:nil];
                     }];
}




- (void)setPushResponseChecked:(NSString *)pushId
                    completion:(void(^)(BOOL))completion {
    
    [self.sessionManager GET:[NSString stringWithFormat:@"%@/api/setPushResponseChecked?user_id=%@&id=%@",
                              kAPIHost,
                              [Common sharedInstance].userId, pushId]
                  parameters:@{}
                    progress:nil
                     success:^(NSURLSessionDataTask * _Nonnull task, NSDictionary * _Nullable responseObject) {
                         if ([[responseObject objectForKey:@"code"] intValue]==CODE_SUCCESS) {
                             completion(YES);
                         } else {
                             // error
                             completion(NO);
                         }
                     }
                     failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                         [[NSNotificationCenter defaultCenter] postNotificationName:@"connectError"
                                                                             object:nil];
                     }];
}



@end
