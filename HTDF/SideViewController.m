//
//  SideViewController.m
//  Cultwo
//
//  Created by ParkJongsung on 2016. 12. 11..
//  Copyright © 2016년 xSync. All rights reserved.
//

#import "SideViewController.h"
#import <ViewDeck/ViewDeck.h>
#import "Common.h"
#import "Functions.h"

#define kHTDFHome @"http://211.249.62.55:8060"

@interface SideViewController ()
@property (weak, nonatomic) IBOutlet UILabel *labelName;
@property (weak, nonatomic) IBOutlet UILabel *labelCompany;

@end

@implementation SideViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.preferredContentSize = CGSizeMake(253, 460);
    
    self.superWebVC = (WebViewController *)self.viewDeckController.centerViewController;
    
    self.viewTicketContainer.layer.shadowOffset = CGSizeMake(0, 2);
    self.viewTicketContainer.layer.shadowColor = [UIColor colorWithWhite:0  alpha:0.2].CGColor;
    self.viewTicketContainer.layer.shadowRadius = 2;
    self.viewTicketContainer.layer.shadowOpacity = 1;

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(setUserDataToLabel:) name:@"setUserDataToLabel" object:nil];
    
    [self setUserDataToLabel:nil];

}

-(void)setUserDataToLabel:(NSNotification *)notif{
    NSString *name = [[Common sharedInstance].userDic objectForKey:@"name"];
    NSString *company = [[[Common sharedInstance] userDic] objectForKey:@"company"];
    
    [self.labelName setText:name];
    
    if( [company nilIfNSNull] ){
        [self.labelCompany setText:company];
    }

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)clickMenuButton:(id)sender {
    [self.viewDeckController closeSide:YES];

}

- (IBAction)clickEntrance:(id)sender {
    [self.superWebVC loadWebsite:[NSString stringWithFormat:@"%@/user/entrance?udid=%@", kHTDFHome, [Functions getDeviceUUID]]];
    self.superWebVC.labelBarTitle.text = @"입장안내";
    
    
    self.superWebVC.containerView.hidden = YES;
    self.superWebVC.containerViewForSecurity.hidden = YES;

    [self.viewDeckController closeSide:YES];
}


- (IBAction)clickNotice:(id)sender {
    [self.superWebVC loadWebsite:[NSString stringWithFormat:@"%@/user/notice", kHTDFHome]];
    self.superWebVC.labelBarTitle.text = @"공지사항";

    self.superWebVC.containerViewForSecurity.hidden = YES;
    self.superWebVC.containerView.hidden = YES;
    
    [self.viewDeckController closeSide:YES];
}

- (IBAction)clickConference:(id)sender {
    [self.superWebVC loadWebsite:[NSString stringWithFormat:@"%@/user/conference", kHTDFHome]];
    self.superWebVC.labelBarTitle.text = @"컨퍼런스 스케줄";

    self.superWebVC.containerViewForSecurity.hidden = YES;
    self.superWebVC.containerView.hidden = YES;
    [self.viewDeckController closeSide:YES];
}

- (IBAction)clickMap:(id)sender {
    self.superWebVC.containerViewForSecurity.hidden = YES;
    self.superWebVC.containerView.hidden = NO;
    
    self.superWebVC.labelBarTitle.text = @"부스배치도";
    [self.viewDeckController closeSide:YES];
}

- (IBAction)clickSpecialFeature:(id)sender {
    [self.superWebVC loadWebsite:[NSString stringWithFormat:@"%@/user/feature", kHTDFHome]];
    self.superWebVC.labelBarTitle.text = @"기획전";

    self.superWebVC.containerViewForSecurity.hidden = YES;
    self.superWebVC.containerView.hidden = YES;
    
    [self.viewDeckController closeSide:YES];

}

- (IBAction)clickSns:(id)sender {
    [self.superWebVC loadWebsite:[NSString stringWithFormat:@"%@/user/sns", kHTDFHome]];
    self.superWebVC.labelBarTitle.text = @"SNS";

    self.superWebVC.containerViewForSecurity.hidden = YES;
    self.superWebVC.containerView.hidden = YES;
    [self.viewDeckController closeSide:YES];
}

- (IBAction)clickFeedback:(id)sender {
    
    [self.superWebVC loadWebsite:[NSString stringWithFormat:@"%@?udid=%@", @"https://xsync.typeform.com/to/zaWakj", [Functions getDeviceUUID]]];
    
    self.superWebVC.labelBarTitle.text = @"피드백 남기기";

    self.superWebVC.containerViewForSecurity.hidden = YES;
    self.superWebVC.containerView.hidden = YES;
    [self.viewDeckController closeSide:YES];
}

- (IBAction)clickSupporter:(id)sender {
    [self.superWebVC loadWebsite:[NSString stringWithFormat:@"%@/user/supporter", kHTDFHome]];
    self.superWebVC.labelBarTitle.text = @"후원 업체";

    self.superWebVC.containerViewForSecurity.hidden = YES;
    self.superWebVC.containerView.hidden = YES;
    [self.viewDeckController closeSide:YES];

}

- (IBAction)clickSecurity:(id)sender {
    self.superWebVC.labelBarTitle.text = @"안전수칙";
    self.superWebVC.containerViewForSecurity.hidden = NO;
    self.superWebVC.containerView.hidden = YES;
    [self.viewDeckController closeSide:YES];
}

- (IBAction)clickContactUs:(id)sender {
    [self.superWebVC loadWebsite:[NSString stringWithFormat:@"%@/user/contactus", kHTDFHome]];
    self.superWebVC.labelBarTitle.text = @"문의처";

    self.superWebVC.containerViewForSecurity.hidden = YES;
    self.superWebVC.containerView.hidden = YES;
    [self.viewDeckController closeSide:YES];

}

- (IBAction)clickBooth:(id)sender {
    [self.superWebVC loadWebsite:[NSString stringWithFormat:@"https://htdfjeju.wordpress.com/"]];
    self.superWebVC.labelBarTitle.text = @"참가업체정보";
    
    
    self.superWebVC.containerView.hidden = YES;
    self.superWebVC.containerViewForSecurity.hidden = YES;
    
    [self.viewDeckController closeSide:YES];
}



@end
