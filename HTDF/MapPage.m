//
//  MapPage.m
//  HTDF
//
//  Created by Haetaek Lee on 2017. 1. 30..
//  Copyright © 2017년 xSync. All rights reserved.
//

#import "MapPage.h"
#import "RequestManager.h"
#import "Functions.h"
//#import "HCSStarRatingView.h"
#import "RatingPopUp.h"
#import "WebViewPage.h"


#define MAP_BG_COLOR [UIColor colorWithWhite:0.94 alpha:1]
#define MAP_BOOTH_COLOR [UIColor colorWithRed:81/255.0 green:52/255.0 blue:133/255.0 alpha:0.7]


@interface MapPage ()
@end

@implementation MapPage


- (void)clickRate:(NSDictionary *)boothData
{
    BOOL isVoted = [[boothData objectForKey:@"isVotedYN"] boolValue];
    if (isVoted) {
        [Functions showAlertWithTitle:@"이미 평가하셨습니다" message:@""];
    } else {
        [RatingPopUp presentPopUpTo:self withData:boothData completion:^{
            NSMutableDictionary *changed = [NSMutableDictionary dictionaryWithDictionary:boothData];
            [changed setObject:@"Y" forKey:@"isVotedYN"];
            for (int i=0 ; i<[_favoriteBoothList count] ; i++) {
                int existId = [_favoriteBoothList[i][@"id"] intValue];
                if (existId == [changed[@"id"] intValue] ) {
                    self.favoriteBoothList[i] = changed;
                    break;
                }
            }
            [self.tableViewSelectedBooth reloadData];
            
            [self loadMyFavoriteList];
        }];
    }
}
- (void)clickDeleteFromFravorite:(NSDictionary *)boothData
{
    [self removeFromFavoriteListWithData:boothData];
}



- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.favoriteBoothList = [NSMutableArray array];
    [self setUserInterface];
    [self loadMapData];
    [self loadMyFavoriteList];
}
- (void)setUserInterface {
    UINib *nib = [UINib nibWithNibName:@"SelectedBoothCell" bundle:nil];
    [_tableViewSelectedBooth registerNib:nib forCellReuseIdentifier:@"SelectedBoothCell"];
    [_tableViewSelectedBooth setRowHeight:36];
    //[_tableViewSelectedBooth setBackgroundColor:<#(UIColor * _Nullable)#>]
    
    self.scrollView.maximumZoomScale = 3.0;
    self.scrollView.minimumZoomScale = 1.0;
    
    self.viewContainer.backgroundColor = MAP_BG_COLOR;
    
    UIButton *resigner = [UIButton buttonWithType:UIButtonTypeCustom];
    resigner.frame = self.viewContainer.bounds;
    [resigner addTarget:self action:@selector(dismissSelectedBoothView) forControlEvents:UIControlEventTouchUpInside];
    [self.viewContainer addSubview:resigner];
}
- (void)loadMapData {
    [[[RequestManager alloc] init] getCompanyInfoListWithCompletion:^(NSArray *results) {
       
        NSArray *infoList = (NSArray *)results;
        self.boothInfoList = infoList;
        
        CGFloat adjustX = -10;
        CGFloat adjustY = -5;
        _scrollView.contentInset = UIEdgeInsetsMake(28, 0, 28, 0);
        
        
        for (int i = 0; i<[infoList count]; i++) {
            NSDictionary *info = infoList[i];
            
            int boothId = [info[@"id"] intValue];
            
            NSString *locationString = info[@"location"];
            NSData *data = [locationString dataUsingEncoding:NSUTF8StringEncoding];
            id json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
            CGRect frame = CGRectMake([json[@"x"] intValue] + adjustX,
                                      [json[@"y"] intValue] + adjustY,
                                      [json[@"width"] intValue],
                                      [json[@"height"] intValue] );
            
            UIView *booth = [[UIView alloc] initWithFrame:frame];
            booth.backgroundColor = MAP_BOOTH_COLOR;
            booth.layer.borderColor = MAP_BG_COLOR.CGColor;
            booth.layer.borderWidth = 1;
            // button 보다 아래에 놓이도록
            //[self.viewContainer addSubview:booth];
            [self.viewContainer insertSubview:booth atIndex:0];
            
            CGRect titleFrame = CGRectInset(booth.bounds, 5, 8);
            UILabel *label = [[UILabel alloc] initWithFrame:titleFrame];
            label.numberOfLines = 0;
            label.backgroundColor = [UIColor clearColor];
            label.text = [NSString stringWithFormat:@"%@\n\n%@", info[@"name"], info[@"location_name"]];
            label.textColor = [UIColor colorWithWhite:1 alpha:1];
            label.font = [UIFont systemFontOfSize:8 weight:UIFontWeightBold];
            //label.minimumScaleFactor = 0.5;
            label.adjustsFontSizeToFitWidth = true;
            label.lineBreakMode = NSLineBreakByCharWrapping;
            [booth addSubview:label];
            
            UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
            [button setImage:[UIImage imageNamed:@"icon_pin"] forState:UIControlStateNormal];
            [button setImage:[UIImage imageNamed:@"icon_pin_yellow"] forState:UIControlStateSelected];
            [button setContentVerticalAlignment:UIControlContentVerticalAlignmentTop];
            [button setContentHorizontalAlignment:UIControlContentHorizontalAlignmentRight];
            [button setClipsToBounds:false];
            [button setContentEdgeInsets:UIEdgeInsetsMake(-3, 0, 0, 3)];
            [button setTag: boothId ];
            [button addTarget:self action:@selector(clickBooth:) forControlEvents:UIControlEventTouchUpInside];
            // 핀이 위에 셀이 있을 경우 셀의 뷰에 의해 가려지는 것 방지
//            [button setFrame: booth.bounds];
//            [booth addSubview:button];
            [button setFrame:booth.frame];
            [self.viewContainer addSubview:button];
        }
        UIImageView *doorUpLeft = [[UIImageView alloc] initWithFrame:CGRectMake(23+adjustX, -28+adjustY +3, 98, 58)];
        doorUpLeft.image = [UIImage imageNamed:@"door_up"];
        doorUpLeft.contentMode = UIViewContentModeScaleAspectFill;
        [self.viewContainer insertSubview:doorUpLeft atIndex:0];
        UIImageView *doorUpCenter = [[UIImageView alloc] initWithFrame:CGRectMake(841+adjustX, -28+adjustY +3, 98, 58)];
        doorUpCenter.image = [UIImage imageNamed:@"door_up"];
        [self.viewContainer insertSubview:doorUpCenter atIndex:0];
        UIImageView *doorDownLeft = [[UIImageView alloc] initWithFrame:CGRectMake(88+adjustX, 608+adjustY +4, 98, 58)];
        doorDownLeft.image = [UIImage imageNamed:@"door_down"];
        [self.viewContainer insertSubview:doorDownLeft atIndex:0];
        UIImageView *doorDownRight = [[UIImageView alloc] initWithFrame:CGRectMake(1419+adjustX, 608+adjustY +4, 98, 58)];
        doorDownRight.image = [UIImage imageNamed:@"door_down"];
        [self.viewContainer insertSubview:doorDownRight atIndex:0];

        
        // favoriteBoothList가 먼저 불러와진 상태일 경우
        if ([self.favoriteBoothList count]>0) {
            [self displayFavoritePin:_favoriteBoothList];
        }
    }];
}
- (void)loadMyFavoriteList {
    [[[RequestManager alloc] init] getFavoriteListWithCompletion:^(NSArray *results) {
        self.favoriteBoothList = [NSMutableArray arrayWithArray:results];
        [self.tableViewSelectedBooth reloadData];
        [self.tableViewSelectedBooth setContentOffset:CGPointZero animated:YES];

        // 아직 안불러와진 상태일 경우
        if ([self.boothInfoList count]>0) {
            [self displayFavoritePin:results];
        }
    }];
}
- (void)displayFavoritePin:(NSMutableArray *)favoriteList {
    for (NSDictionary *data in favoriteList) {
        int boothId = [data[@"id"] intValue];
        [(UIButton *)[self.viewContainer viewWithTag:boothId] setSelected:YES];
    }
}

- (void)clickBooth:(UIButton *)sender {
    int boothId = (int)sender.tag;
    NSDictionary *info = [self getBoothDataOfId:boothId];
    
    BOOL addToList = !sender.isSelected;
    if (addToList)
    {
        [self addToFavoriteListWithData:info];
    }
    else
    {
        [self removeFromFavoriteListWithData:info];
    }
}
- (void)addToFavoriteListWithData:(NSDictionary *)boothData {
    int boothId = [boothData[@"id"] intValue];
    [[[RequestManager alloc] init] addToFavoriteListWithId:boothId completion:^(BOOL success) {
        [[self.viewContainer viewWithTag:boothId] setSelected:YES];
        [self presentSelectedBoothView];
        //
        [_favoriteBoothList insertObject:boothData atIndex:0];
        [_tableViewSelectedBooth reloadData];
        // 평가했는지 여부의 데이터는 여기에만 들어 있음
        [self loadMyFavoriteList];
    }];
}
- (void)removeFromFavoriteListWithData:(NSDictionary *)boothData {
    int boothId = [boothData[@"id"] intValue];
    [[[RequestManager alloc] init] removeFromFavoriteListWithId:boothId completion:^(BOOL success) {
        [[self.viewContainer viewWithTag:boothId] setSelected:NO];
        [self presentSelectedBoothView];
        //
        [_favoriteBoothList removeObject:boothData];
        [_tableViewSelectedBooth reloadData];
        // 평가했는지 여부의 데이터는 여기에만 들어 있음
        [self loadMyFavoriteList];
    }];
}


- (void)presentSelectedBoothView {
    [UIView animateWithDuration:0.3 delay:0 options:UIViewAnimationOptionBeginFromCurrentState animations:^{
        self.constraintSelectedViewBottom.constant = 0;
        [self.view layoutIfNeeded];
    } completion:nil];
    
}
- (void)dismissSelectedBoothView {
    [UIView animateWithDuration:0.3 delay:0 options:UIViewAnimationOptionBeginFromCurrentState animations:^{
        self.constraintSelectedViewBottom.constant = -_viewSelectedBooth.frame.size.height;
        [self.view layoutIfNeeded];
    } completion:nil];
}


- (NSDictionary *)getBoothDataOfId:(int)boothId {
    for (NSDictionary *boothData in self.boothInfoList) {
        if ([boothData[@"id"] intValue]==boothId) {
            return boothData;
        }
    }
    return nil;
}




- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *favoriteBoothData = _favoriteBoothList[indexPath.row];
    NSString *urlString = favoriteBoothData[@"url"];
    if ([urlString nilIfNSNull] && [urlString length]) {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        UINavigationController *navigationController = [storyboard instantiateViewControllerWithIdentifier:@"WebViewNavigation"];
        WebViewPage *webViewPage = navigationController.viewControllers.firstObject;
        webViewPage.url = urlString;
        [self presentViewController:navigationController animated:YES completion:nil];
    }
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    SelectedBoothCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SelectedBoothCell" forIndexPath:indexPath];
    cell.protocolDelegate = self;
    
    NSDictionary *favoriteBoothData = _favoriteBoothList[indexPath.row];
    [cell setData:favoriteBoothData indexPath:indexPath];
    return cell;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [_favoriteBoothList count];
}



- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if ([scrollView isEqual:self.scrollView]) {
        [self dismissSelectedBoothView];
    }
}
- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView
{
    return self.viewContainer;
}


@end
