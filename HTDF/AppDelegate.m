//
//  AppDelegate.m
//  SK Knights
//
//  Created by 김기범 on 2016. 10. 13..
//  Copyright © 2016년 xSync. All rights reserved.
//

#import "AppDelegate.h"
#import "ATAppUpdater/ATAppUpdater.h"
#import "RatingPopUp.h"
#import "Functions.h"
#import "Common.h"
#import "RequestManager.h"


@interface AppDelegate ()
@property NSString *name;
@property NSString *phone;
@end

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application
    didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    /* Register Push Notification */
    [[UIApplication sharedApplication] registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge) categories:nil]];
   
    [UIApplication sharedApplication].idleTimerDisabled = YES;

    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleLightContent;
    
    ATAppUpdater *updater = [ATAppUpdater sharedUpdater];
    [updater setAlertTitle:@"업데이트"];
    [updater setAlertMessage:@"새로운 버전의 앱이 존재합니다.\n마켓에서 앱 업데이트해주세요.\n업데이트 하지 않으면, 사용 가능한 기능이 제한될 수 있습니다."];
    [updater setAlertUpdateButtonTitle:@"업데이트"];
    [updater showUpdateWithForce];

    NSDictionary *userInfo = [launchOptions objectForKey:UIApplicationLaunchOptionsRemoteNotificationKey];

    if( userInfo ){
        if( [userInfo objectForKey:@"type"] &&
           [[userInfo objectForKey:@"type"] hasPrefix:@"normal"]){
            [self didReceiveNormalNotification:userInfo];
        }
    }
    return YES;
}

- (void)application:(UIApplication *)application didRegisterUserNotificationSettings:(UIUserNotificationSettings *)notificationSettings { // NS_AVAILABLE_IOS(8_0);
    [application registerForRemoteNotifications];
}

- (void)application:(UIApplication*)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData*)deviceToken{
    NSMutableString *deviceId = [NSMutableString string];
    const unsigned char* ptr = (const unsigned char*) [deviceToken bytes];
    for(int i = 0 ; i < 32 ; i++)
    {
        [deviceId appendFormat:@"%02x", ptr[i]];
    }
    NSLog(@"PUSH TOKEN IS %@", deviceId);
    
    [[NSUserDefaults standardUserDefaults] setObject:deviceId forKey:@"xSync_pushToken"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}


-(void)applicationDidBecomeActive:(UIApplication *)application{
    application.applicationIconBadgeNumber = 0;
    
    if([Common sharedInstance].userId){
        RequestManager *requestManager = [[RequestManager alloc] init];
        [requestManager checkVovWithCompletion:^(NSDictionary *data) {
            [RatingPopUp presentPopUpTo:[Functions topMostController]
                            withVOVData:data
                             completion:^{
                                 
                             }];
        }];
    }
    
}
-(void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo{
    if(application.applicationState == UIApplicationStateActive){
        if([[userInfo objectForKey:@"type"] hasPrefix:@"vov"]){
            [RatingPopUp presentPopUpTo:[Functions topMostController]
                            withVOVData:userInfo
                             completion:^{
                                 
                             }];
            
        }
    }
    
    if( [userInfo objectForKey:@"type"] &&
       [[userInfo objectForKey:@"type"] hasPrefix:@"normal"] ){
        [self didReceiveNormalNotification:userInfo];
    }
}


-(void)didReceiveNormalNotification:(NSDictionary *)userInfo{
    RequestManager *requestManager = [[RequestManager alloc] init];
    [requestManager setPushResponseChecked:[userInfo objectForKey:@"id"]
                completion:^(BOOL isCompleted) {
                    
                }];
}


- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
}


- (void)applicationWillTerminate:(UIApplication *)application {
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"is_seatInputted"];
}


@end
