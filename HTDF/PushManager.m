//
//  SocketManager.m
//  Cultwo
//
//  Created by ParkJongsung on 2017. 1. 9..
//  Copyright © 2017년 xSync. All rights reserved.
//

#import "PushManager.h"
// #import "Cultwo-Swift.h"
#import "EmbeddedAppViewController.h"
#import "Functions.h"



@interface PushManager()


@property(weak, nonatomic) EmbeddedAppViewController  * _Nullable vovVC;
@property(weak, nonatomic) UIViewController * _Nullable parentVC;


@end

@implementation PushManager


-(instancetype)initWithParentVC:(UIViewController *)parentVC
                    vovVC:(EmbeddedAppViewController *)vovVC{
    self = [super init];
    if( self ){
        
        
    
//        [_socket on:@"xSync:general:getCurrentStatusFromApp"
//           callback:^(NSArray *data, SocketAckEmitter *ack) {
//               
//               dispatch_async(dispatch_get_main_queue(), ^{
//                   NSDictionary *returnData = [data objectAtIndex:0];
//                   NSString *statusType = returnData[@"resultObject"][@"status"];
//                   //STATUS별 분기.
//                   
//                   if( [statusType isEqualToString:@"STATUS_CARD"] ){
//                       NSString *timeToStart = [[data objectAtIndex:0][@"resultObject"][@"info"] objectForKey:@"startTime"];
//                       NSString *dataArr = [[data objectAtIndex:0][@"resultObject"][@"info"] objectForKey:@"colorArr"];
//                       NSString *interval =  [[data objectAtIndex:0][@"resultObject"][@"info"] objectForKey:@"interval"];
//                       NSString *cardMode = [[data objectAtIndex:0][@"resultObject"][@"info"] objectForKey:@"cardMode"];
//                       
//                       NSString *level = [[data objectAtIndex:0][@"resultObject"][@"info"] objectForKey:@"level"];
//                       
//                       if( dataArr != nil
//                          && ![dataArr isEqualToString:@""] ){
//                           
//                           [_cardManager startTickInCardSection:level
//                                                        dataArr:[dataArr componentsSeparatedByString:@","]
//                                                       interval:interval
//                                                       cardMode:cardMode
//                                                       withTime:timeToStart];
//                       }
//                   }else if([statusType isEqualToString:@"STATUS_EMBEDED"]){
//                       //앱위에 앱 중간에 켤때.
//                       NSString *url = [[data objectAtIndex:0][@"resultObject"][@"info"] objectForKey:@"url"];
//                       [self _loadEmbededWebPageWithURLString:url];
//                   }
//               });
//           }];
//        
//        [_socket on:@"xSync:admin:startProgram" callback:^(NSArray *data, SocketAckEmitter *ack) {
//            NSDictionary *returnData = [data objectAtIndex:0];
//            NSString *eventType = returnData[@"resultObject"][@"info"][@"eventType"];
//            
//            if( [eventType isEqualToString:@"card"] ){
//                NSString *timeToStart = [[data objectAtIndex:0][@"resultObject"] objectForKey:@"destTime"];
//                NSString *interval =  [[data objectAtIndex:0][@"resultObject"][@"info"] objectForKey:@"interval"];
//                NSString *cardMode = [[data objectAtIndex:0][@"resultObject"][@"info"] objectForKey:@"cardMode"];
//                NSString *level = [[data objectAtIndex:0][@"resultObject"][@"info"] objectForKey:@"level"];
//                [_cardManager startTickInCardSection:level
//                                            interval:interval
//                                            cardMode:cardMode
//                                            withTime:timeToStart];
//                
//            }else if( [eventType isEqualToString:@"embeded"]){
//                // 앱위에 앱 켤때 .
//                // 보여줄 URL
//                NSString *url = [[data objectAtIndex:0][@"resultObject"][@"info"] objectForKey:@"url"];
//                NSLog(@"url %@", url);
//                [self _loadEmbededWebPageWithURLString:url];
//            }
//            
//        }];
        
    }
    return self;
}


-(void)_loadEmbededWebPageWithURLString:(NSString *)string{
    if( [_vovVC presentingViewController] ){
        [_vovVC loadWebPageWithURLString:string];
    }else {
        [self _presentEmbededViewControllerWithURLString:string];
    }
}



-(void)_presentEmbededViewControllerWithURLString:(NSString *)url{
    _vovVC.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
    [_parentVC presentViewController:_vovVC
                       animated:YES
                     completion:^{
                         [_vovVC loadWebPageWithURLString:url];
                     }];
}


-(void)_closeEmbededWebPage{
    if( [_vovVC presentingViewController]){
        [_vovVC dismissViewControllerAnimated:YES completion:^{
            
        }];
    }
}

-(void)_closeEmbededWebPageWithBlock:(void (^)(void))block{
    if( [_vovVC presentingViewController]){
        [_vovVC dismissViewControllerAnimated:YES completion:^{
            if( block != nil ){
                block();
            }
        }];
    }
}




@end
