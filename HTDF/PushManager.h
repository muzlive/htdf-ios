//
//  SocketManager.h
//  Cultwo
//
//  Created by ParkJongsung on 2017. 1. 9..
//  Copyright © 2017년 xSync. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@class EmbeddedAppViewController;

@interface PushManager : NSObject

-(nonnull id)initWithParentVC:(nonnull UIViewController *)parentVC 
                        vovVC:(nonnull EmbeddedAppViewController *)vovVC;


@end
