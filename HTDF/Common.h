//
//  Common.h
//  Cultwo
//
//  Created by ParkJongsung on 2016. 12. 11..
//  Copyright © 2016년 xSync. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Functions.h"

@interface Common : NSObject

+ (Common *)sharedInstance;

@property (nonatomic, strong) NSString *currentPerformanceSeq;
@property (nonatomic, strong) NSString *userId;
@property (nonatomic, strong) NSDictionary *userDic;


@end
