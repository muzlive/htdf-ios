//
//  SecondLaunchScreenController.m
//  SK Knights
//
//  Created by 김기범 on 2016. 10. 15..
//  Copyright © 2016년 xSync. All rights reserved.
//

#import "SecondLaunchScreenController.h"
#import "WebViewController.h"
#import <ViewDeck/ViewDeck.h>
#import "SideViewController.h"
#import "RequestManager.h"
#import "Common.h"
#import "Functions.h"


@interface SecondLaunchScreenController ()
@property (strong, nonatomic) IBOutlet UIView *cultwoLogoView;
@property (weak, nonatomic) IBOutlet UIImageView *logoImageView1;
@property (weak, nonatomic) IBOutlet UIImageView *logoImageView2;
@end

CGFloat const secondShowTime = 1.4;
CGFloat const dismissTime = 0.8;

@implementation SecondLaunchScreenController

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)viewDidAppear:(BOOL)animated {
    dispatch_time_t secondShowDispatchTime = dispatch_time(DISPATCH_TIME_NOW,
                                                           (int64_t)(secondShowTime * NSEC_PER_SEC));
    dispatch_after(secondShowDispatchTime, dispatch_get_main_queue(), ^{
        [self showNextImage];
    });
    
    RequestManager *requestManager=  [[RequestManager alloc] init];
    
    // 최초에 Performance Data 불러옴
    [requestManager getCurrentPerformanceDataWithCompletion:^(NSDictionary * result) {
        [[Common sharedInstance] setCurrentPerformanceSeq:
         [result objectForKey:@"currentPerformanceSeq"]];
        
    }];
    
}

-(void)showNextImage{
    [UIView animateWithDuration:0.2 animations:^{
        self.logoImageView1.alpha = 0;
        self.logoImageView2.alpha = 1;
    }];
    dispatch_time_t dismissDispatchTime = dispatch_time(DISPATCH_TIME_NOW,
                                                        (int64_t)((dismissTime) * NSEC_PER_SEC));
    dispatch_after(dismissDispatchTime, dispatch_get_main_queue(), ^{
        UIViewController *controller = [self controllerToShow];
        CATransition *transition = [CATransition animation];
        transition.duration = 0.35;
        transition.type = kCATransitionFade;
        [self.view.layer addAnimation:transition forKey:kCATransition];
        controller.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
        [self presentViewController:controller
                           animated:YES
                         completion:nil];
    });
}


- (UIViewController *)controllerToShow {

    UIViewController *centerViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"web_app"];
    
    SideViewController *sideViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"side_vc"];
    
    IIViewDeckController *viewDeckController = [[IIViewDeckController alloc]
                                                initWithCenterViewController:centerViewController leftViewController:sideViewController];
    // viewDeckController.preferredContentSize = CGSizeMake(, )
    return viewDeckController;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}


@end
