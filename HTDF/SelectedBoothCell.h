//
//  SelectedBoothCell.h
//  HTDF
//
//  Created by Haetaek Lee on 2017. 2. 12..
//  Copyright © 2017년 xSync. All rights reserved.
//

#import <UIKit/UIKit.h>


@protocol SelectedBoothCellProtocol <NSObject>
- (void)clickDeleteFromFravorite:(NSDictionary *)boothData;
- (void)clickRate:(NSDictionary *)boothData;
@end


@interface SelectedBoothCell : UITableViewCell

@property (nonatomic, strong) NSDictionary *boothData;
@property (nonatomic, assign) id<SelectedBoothCellProtocol> protocolDelegate;
- (void)setData:(NSDictionary *)data indexPath:(NSIndexPath *)indexPath;

    
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *loading;
@property (weak, nonatomic) IBOutlet UILabel *labelAddress;
@property (weak, nonatomic) IBOutlet UILabel *labelTitle;
@property (weak, nonatomic) IBOutlet UIButton *buttonRate;
- (IBAction)clickRate:(id)sender;
- (IBAction)clickDelete:(id)sender;

@end
