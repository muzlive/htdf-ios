//
//  SelectedBoothCell.m
//  HTDF
//
//  Created by Haetaek Lee on 2017. 2. 12..
//  Copyright © 2017년 xSync. All rights reserved.
//

#import "SelectedBoothCell.h"

@implementation SelectedBoothCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (IBAction)clickRate:(id)sender {
    if ([self.protocolDelegate respondsToSelector:@selector(clickRate:)]) {
        [self.protocolDelegate clickRate:self.boothData];
    }
}

- (IBAction)clickDelete:(id)sender {
    if ([self.protocolDelegate respondsToSelector:@selector(clickDeleteFromFravorite:)]) {
        [self.protocolDelegate clickDeleteFromFravorite:self.boothData];
    }
}


- (void)setData:(NSDictionary *)data indexPath:(NSIndexPath *)indexPath
{
    self.boothData = data;
    
    self.labelTitle.text = [data objectForKey:@"name"];
    self.labelAddress.text = [data objectForKey:@"location_name"];
    id isVotedValue = [data objectForKey:@"isVotedYN"];
    if (isVotedValue != nil) {
        [self.buttonRate setHidden:NO];
        [self.loading stopAnimating];
        
        BOOL isVoted = [isVotedValue boolValue];
        [self.buttonRate setSelected:isVoted];
    } else {
        [self.buttonRate setHidden:YES];
        [self.loading startAnimating];
    }
}

@end
