//
//  Functions.m
//  Qfeed
//
//  Created by MacBook on 11. 5. 21..
//  Copyright 2011 POSTECH. All rights reserved.
//

#import "Functions.h"
@import StoreKit;


@implementation Functions


NSString* deviceName()
{
    struct utsname systemInfo;
    uname(&systemInfo);
    
    return [NSString stringWithCString:systemInfo.machine
                              encoding:NSUTF8StringEncoding];
}

CGFloat yBottomOfStatusBar()
{
    CGFloat maxY = 0;
    
    UIInterfaceOrientation statusBarOrientation = [[UIApplication sharedApplication] statusBarOrientation];
    if ([UIApplication sharedApplication].statusBarHidden)
    {
        maxY = 0;
    }
    else
    {
        if ( SYSTEM_VERSION_LESS_THAN(@"7.0") )
        {
            maxY = 0;
        }
        else
        {
            if ( statusBarOrientation==UIInterfaceOrientationPortrait ||
                statusBarOrientation==UIInterfaceOrientationPortraitUpsideDown) {
                maxY = CGRectGetMaxY([UIApplication sharedApplication].statusBarFrame);
            } else {
                if ( SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0") ) {
                    maxY = [UIApplication sharedApplication].statusBarFrame.size.height;
                } else {
                    maxY = [UIApplication sharedApplication].statusBarFrame.size.width;
                }
            }
        }
    }
    return maxY;
}


void runOnMainQueueWithoutDeadlocking(void (^block)(void))
{
    if ([NSThread isMainThread])
    {
        block();
    }
    else
    {
        dispatch_sync(dispatch_get_main_queue(), block);
    }
}

//
// 싱글톤
//
+ (Functions *)sharedInstance
{
    static dispatch_once_t once;
    static Functions *__sharedInstance = nil;
    if (!__sharedInstance)
    {
        // @synchronized([Functions class]) {
        // : 메소드에 synchronized를 걸 수 있는 것이 아니므로, 다른 언어와 다르게 DCL(Double-checked locking)이 등장하지는 않음
        //   @synchronized 대신 앱의 생명주기에 블럭객체를 단 한번 생성을 보장하는 dispatch_once 사용
        //
        dispatch_once(&once, ^{
            //
            // __sharedInstance = [[Functions alloc] init];
            // : 위처럼 할 경우 서브클래싱 시 서브클래싱된 인스턴스가 아니라 Functions 인스턴스가 생성
            //
            __sharedInstance = [[[self class] alloc] init];
        });
    }
    return __sharedInstance;
}



/*
// audioPlayer
static char audioPlayerKey;
+ (AVAudioPlayer *)audioPlayerWithRetainObject:(id)retainObject
{
    if (retainObject==nil) {
        retainObject = [[UIApplication sharedApplication]delegate];
    }
    AVAudioPlayer *audioPlayer = objc_getAssociatedObject(retainObject, &audioPlayerKey);
    return audioPlayer;
}
+ (void)audioPlayerWithRetainObject:(id)retainObject playURL:(NSURL *)url volume:(CGFloat)volume numberOfLoops:(NSInteger)numberOfLoops
{
    if (retainObject==nil) {
        retainObject = [[UIApplication sharedApplication]delegate];
    }
    
    AVAudioPlayer *audioPlayer = objc_getAssociatedObject(retainObject, &audioPlayerKey);
    if (audioPlayer) {
        [audioPlayer stop];
        [audioPlayer release], audioPlayer=nil;
    }
    NSError *error;
    audioPlayer = [[AVAudioPlayer alloc]initWithContentsOfURL:url error:&error];
    [audioPlayer setNumberOfLoops:numberOfLoops];
    [audioPlayer setVolume:volume];
    [audioPlayer prepareToPlay];
    [audioPlayer play];
    
    objc_setAssociatedObject(retainObject, &audioPlayerKey, audioPlayer, OBJC_ASSOCIATION_RETAIN);
}
*/
/*
+ (BOOL)trimAudioWithInputURL:(NSURL *)audioFileInput outPutURL:(NSURL *)audioFileOutput startFrom:(float)vocalStartMarker endTo:(float)vocalEndMarker
{
    __block BOOL returnValue = NO;
    //    float vocalStartMarker = <starting time>;
    //    float vocalEndMarker = <ending time>;
    
    //    NSURL *audioFileInput = <your pre-existing file>;
    //    NSURL *audioFileOutput = <the file you want to create>;
    
    if (!audioFileInput || !audioFileOutput)
    {
        return NO;
    }
    
    [[NSFileManager defaultManager] removeItemAtURL:audioFileOutput error:NULL];
    AVAsset *asset = [AVAsset assetWithURL:audioFileInput];
    
    AVAssetExportSession *exportSession = [AVAssetExportSession exportSessionWithAsset:asset
                                                                            presetName:AVAssetExportPresetAppleM4A];
    
    if (exportSession == nil)
    {
        return NO;
    }
    
    CMTime startTime = CMTimeMake((int)(floor(vocalStartMarker * 100)), 100);
    CMTime stopTime = CMTimeMake((int)(ceil(vocalEndMarker * 100)), 100);
    CMTimeRange exportTimeRange = CMTimeRangeFromTimeToTime(startTime, stopTime);
    
    exportSession.outputURL = audioFileOutput;
    exportSession.outputFileType = AVFileTypeAppleM4A;
    exportSession.timeRange = exportTimeRange;
    
    CFRunLoopRef currentLoop = CFRunLoopGetCurrent();
    [exportSession exportAsynchronouslyWithCompletionHandler:^
     {
         if (AVAssetExportSessionStatusCompleted == exportSession.status)
         {
             NSLog(@"complete");
             // It worked!
             returnValue = YES;
         }
         else if (AVAssetExportSessionStatusFailed == exportSession.status)
         {
             NSLog(@"fail");
             // It failed...
             returnValue = NO;
         }
         CFRunLoopStop(currentLoop);
     }];
    
    CFRunLoopRun();
    currentLoop = CFRunLoopGetCurrent();
    
    return returnValue;
}
*/

+ (void)saveDataToPlist:(id)data forKey:(NSString *)key
{
    NSData *_dataForSave = [NSKeyedArchiver archivedDataWithRootObject:data];
    [[NSUserDefaults standardUserDefaults]setObject:_dataForSave forKey:key];
    [[NSUserDefaults standardUserDefaults]synchronize];
}
+ (id)loadDataFromPlistForKey:(NSString *)key
{
    NSData *_savedData = [[NSUserDefaults standardUserDefaults]objectForKey:key];
    id data = [NSKeyedUnarchiver unarchiveObjectWithData:_savedData];
    
    return data;
}
+ (void)removeAllDataInPlist
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSArray *keys = [[defaults dictionaryRepresentation] allKeys];
    for(NSString *key in keys) {
        [defaults removeObjectForKey:key];
    }
    [defaults synchronize];
}


+ (void)saveArrayToPlist:(NSMutableArray *)array forKey:(NSString *)key
{
    NSData *_dataForSave = [NSKeyedArchiver archivedDataWithRootObject:array];
    [[NSUserDefaults standardUserDefaults]setObject:_dataForSave forKey:key];
    [[NSUserDefaults standardUserDefaults]synchronize];
}
+ (NSMutableArray *)loadArrayFromPlistForKey:(NSString *)key
{
    NSData *_savedData = [[NSUserDefaults standardUserDefaults]objectForKey:key];
    NSMutableArray *array = [NSKeyedUnarchiver unarchiveObjectWithData:_savedData];

    return [NSMutableArray arrayWithArray:array];
}
+ (void)removeAllObjectsInArrayFromPlistForKey:(NSString *)key
{
    NSData *_savedData = [[NSUserDefaults standardUserDefaults]objectForKey:key];
    NSMutableArray *array = [NSKeyedUnarchiver unarchiveObjectWithData:_savedData];
    
    [array removeAllObjects];
    [Functions saveArrayToPlist:array forKey:key];
}





static char blockKeyMailComposeDelegate;
+ (MFMailComposeViewController *)presentMailComposeViewControllerTo:(UIViewController *)superViewController
                                                       toRecipients:(NSArray *)toRecipients
                                                            subject:(NSString *)subject
                                                        messageBody:(NSString *)messageBody
{
    return [Functions presentMailComposeViewControllerTo:superViewController animated:YES completion:nil
                                            toRecipients:toRecipients subject:subject messageBody:messageBody delegate:nil];
}
+ (MFMailComposeViewController *)presentMailComposeViewControllerTo:(UIViewController *)superViewController
                                                           animated:(BOOL)animated
                                                         completion:(void(^)())completion
                                                       toRecipients:(NSArray *)toRecipients
                                                            subject:(NSString *)subject
                                                        messageBody:(NSString *)messageBody
                                                           delegate:(void(^)(MFMailComposeViewController *controller, MFMailComposeResult result, NSError *error))delegate
{
    if ([MFMailComposeViewController canSendMail])
	{
        if (messageBody==nil)
        {
            messageBody = [NSString stringWithFormat:@"Bundle version : %@\nModel : %@\niOS Version : %@\n------------------------------\n\n\n"
                           ,[[NSBundle mainBundle]objectForInfoDictionaryKey:@"CFBundleVersion"]
                           , deviceName()// ,[[UIDevice currentDevice] model]
//                           ,[[UIDevice currentDevice] name] -> 2rr5r의 iPhone
//                           ,[[UIDevice currentDevice] systemName] -> Mac OS X or OS X
                           ,[[UIDevice currentDevice] systemVersion]
                           ];
        }
        
		MFMailComposeViewController *mailViewController = [[MFMailComposeViewController alloc] init];
		mailViewController.mailComposeDelegate = (id <MFMailComposeViewControllerDelegate>)[self class];
		[mailViewController setToRecipients:toRecipients];
		[mailViewController setSubject:subject];
        [mailViewController setMessageBody:messageBody isHTML:NO];
        [superViewController presentViewController:mailViewController animated:animated completion:completion];

        if (delegate) {
            objc_setAssociatedObject(mailViewController, &blockKeyMailComposeDelegate, delegate, OBJC_ASSOCIATION_COPY);
        }
        return mailViewController;
	}
	else
	{
		NSLog(@"Device is unable to send email in its current state.");
        return nil;
	}
}
+ (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{
    void(^delegateBlock)(MFMailComposeViewController *controller, MFMailComposeResult result, NSError *error) = objc_getAssociatedObject(controller, &blockKeyMailComposeDelegate);
    if (delegateBlock)
    {
        delegateBlock(controller, result, error);
    }
    else
    {
        [controller dismissViewControllerAnimated:YES completion:nil];
    }
}


static char blockKeyMessageComposeDelegate;
+ (MFMessageComposeViewController *)presentMessageComposeViewControllerTo:(UIViewController *)superViewController
                                                               recipients:(NSArray *)recipient
                                                                     body:(NSString *)body
                                                                 delegate:(void(^)(MessageComposeResult result))delegate
{
    return [Functions presentMessageComposeViewControllerTo:superViewController animated:YES completion:nil
                                                 recipients:recipient body:body delegate:delegate];
}
+ (MFMessageComposeViewController *)presentMessageComposeViewControllerTo:(UIViewController *)superViewController
                                                                 animated:(BOOL)animated
                                                               completion:(void(^)())completion
                                                               recipients:(NSArray *)recipient
                                                                     body:(NSString *)body
                                                                 delegate:(void(^)(MessageComposeResult result))delegate
{
    MFMessageComposeViewController *messageViewController = [[MFMessageComposeViewController alloc] init];
    if([MFMessageComposeViewController canSendText])
    {
        messageViewController.body = body;
        messageViewController.recipients = recipient;
        messageViewController.messageComposeDelegate = (id <MFMessageComposeViewControllerDelegate>)[self class];
        messageViewController.delegate = (id <UINavigationControllerDelegate>)self;
        [superViewController presentViewController:messageViewController animated:animated completion:completion];
        
        if (delegate) {
            objc_setAssociatedObject(messageViewController, &blockKeyMessageComposeDelegate, delegate, OBJC_ASSOCIATION_COPY);
        }
    }
    return messageViewController;
}
+ (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result
{
    void(^delegateBlock)(MessageComposeResult result) = objc_getAssociatedObject(controller, &blockKeyMessageComposeDelegate);
    if (delegateBlock)
    {
        delegateBlock(result);
    }
    else
    {
        switch (result)
        {
            case MessageComposeResultCancelled:
                break;
            case MessageComposeResultFailed:
                break;
            case MessageComposeResultSent:
                
                break;
            default:
                break;
        }
        
        [controller dismissViewControllerAnimated:YES completion:nil];
    }
}




+ (NSString *)timestampString
{
    NSDateFormatter *dateFormatter=[[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd hh:mm:ss"];
    [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
    
    NSString *currentDateTime = [dateFormatter stringFromDate:[NSDate date]];
    return currentDateTime;
}
+ (NSString *)convertTommssFormatStringFromSeconds:(NSInteger)totalSec
{
    NSString *timeFormatString = nil;
    
    int remainSec = (int)totalSec;
    int min = remainSec/60;
    int sec = remainSec%60;
    if (min<10)
    {
        if (sec<10) {
            timeFormatString = [NSString stringWithFormat:@"0%d:0%d",min,sec];
        } else {
            timeFormatString = [NSString stringWithFormat:@"0%d:%d",min,sec];
        }
    }
    else
    {
        if (sec<10) {
            timeFormatString = [NSString stringWithFormat:@"%d:0%d",min,sec];
        } else {
            timeFormatString = [NSString stringWithFormat:@"%d:%d",min,sec];
        }
    }
    return timeFormatString;
}
+ (NSString *)convertToHHmmssFormatStringFromSeconds:(NSInteger)totalSec
{
    NSString *hourString = @"00";
    NSString *minuteString = @"00";
    NSString *secondString = @"00";
    int hour = 0;
    int minute = 0;
    int second = 0;
    
    if (totalSec>=60*60)
    {
        hour = (int)totalSec/(60*60);
        totalSec -= hour*(60*60);
        if (hour<10) {
            hourString = [NSString stringWithFormat:@"0%d",hour];
        } else {
            hourString = [NSString stringWithFormat:@"%d",hour];
        }
    }

    if (totalSec>=60)
    {
        minute = (int)totalSec/60;
        totalSec -= minute*60;
        if (minute<10) {
            minuteString = [NSString stringWithFormat:@"0%d",minute];
        } else {
            minuteString = [NSString stringWithFormat:@"%d",minute];
        }
    }
    
    if (1)
    {
        second = (int)totalSec;
        if (second<10) {
            secondString = [NSString stringWithFormat:@"0%d",second];
        } else {
            secondString = [NSString stringWithFormat:@"%d",second];
        }
    }
    
    return [NSString stringWithFormat:@"%@시간 %@분 %@초", hourString, minuteString, secondString];
}
+ (NSString *)convertToddHHmmssFormatStringFromSeconds:(NSInteger)totalSec
{
	NSString *intervalString = @"";
    
	int min = 0;
	int hour = 0;
	int day = 0;
	if (totalSec >= 60*60*24) {
		day = (int)totalSec/(60*60*24);
		totalSec -= (60*60*24)*day;
		intervalString = [intervalString stringByAppendingFormat:@"%d일 ",day];
	}
	if (totalSec >= 60*60) {
		hour  = (int)totalSec/(60*60);
		totalSec -= (60*60)*hour;
		intervalString = [intervalString stringByAppendingFormat:@"%d시간 ",hour];
	}
	if (totalSec >= 60) {
		min = (int)totalSec/60;
		totalSec -= 60*min;
		intervalString = [intervalString stringByAppendingFormat:@"%d분 ",min];
	}
	intervalString = [intervalString stringByAppendingFormat:@"%ld초",(long)totalSec];
	
	return intervalString;
}


+(NSString *) getDateString:(NSString *)date {
    
    NSDate *nowDate = [NSDate date];
    
    NSString *locale = [[NSLocale preferredLanguages] objectAtIndex:0];
    
    NSDateFormatter * timeDateFormatter = [[NSDateFormatter alloc] init];
    [timeDateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss.0"];
    [timeDateFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:locale]];
    
    
    NSDateFormatter * dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:locale]];
    [dateFormatter setDateFormat:@"yyyyMMdd"];
    
    
    NSDateFormatter *timeFormatter = [[NSDateFormatter alloc] init];
    [timeFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:locale]];
    [timeFormatter setDateFormat:NSLocalizedString(@"시간계산",)];
    
    NSDateFormatter * yearDateFormatter = [[NSDateFormatter alloc] init];
    [yearDateFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:locale]];
    [yearDateFormatter setDateFormat:NSLocalizedString(@"날짜계산-1년미만",)];
    
    
    NSDateFormatter *yearDateFormatter2 = [[NSDateFormatter alloc] init];
    [yearDateFormatter2 setLocale:[[NSLocale alloc] initWithLocaleIdentifier:locale] ];
    [yearDateFormatter2 setDateFormat:NSLocalizedString(@"날짜계산-1년이상",)];
    
    
    
    NSDate *addDate = [timeDateFormatter dateFromString:date];
    NSTimeInterval timeInterval = [nowDate timeIntervalSinceDate:addDate];
    
    
    
    NSString *strData = nil;
    if (timeInterval < 60) {
        //60초 미만
        strData = NSLocalizedString(@"방금",);
    } else if (timeInterval < 120) {
        // 1분
        strData = NSLocalizedString(@"1분 전",);
    } else if (timeInterval < 3600) {
        // 1시간 미만
        strData = [NSString stringWithFormat:@"%d %@", (int)(timeInterval / 60), NSLocalizedString(@"분 전",)];
    } else if (timeInterval < 7200) {
        // 1시간
        strData = NSLocalizedString(@"1시간 전",);
    } else if (timeInterval < 86400) {
        // 24시간 미만
        strData = [NSString stringWithFormat:@"%d %@", (int)(timeInterval / 3600), NSLocalizedString(@"시간 전",)];
    } else if (timeInterval < 172800) {
        
        int valueOfToday = [[dateFormatter stringFromDate:nowDate] intValue]; //오늘날짜
        int valueOfTheDate = [[dateFormatter stringFromDate:addDate] intValue]; //입력날짜
        int valueInterval = valueOfToday - valueOfTheDate; //두 날짜 차이
        if(valueInterval == 1) {
            strData = [NSString stringWithFormat:@"%@ %@", NSLocalizedString(@"어제",),
                       [timeFormatter stringFromDate:addDate]];
        }
    }
    
    
    
    if (strData == nil) {
        if (timeInterval < 31536000) {
            // 1년 미만
            strData = [yearDateFormatter stringFromDate:addDate];
        } else {
            strData = [yearDateFormatter2 stringFromDate:addDate];
        }
    }
    
    return strData;
}




/*
// sortUsingComparator: 메서드 참고용 예제로 남겨두기
+ (void)sortArray:(NSMutableArray *)unsortedDatesArray
{
    //	NSArray *sortedArray = [unsortedDatesArray sortedArrayUsing:@selector(compare)];	
	
	[unsortedDatesArray sortUsingComparator:^NSComparisonResult(id obj1, id obj2) {	
		// NSString으로 된 키로 정렬
        //		NSDate *date1 = [Functions convertStringToDate: [(NSMutableDictionary *)obj1 objectForKey:@"date"] ];
        //		NSDate *date2 = [Functions convertStringToDate: [(NSMutableDictionary *)obj2 objectForKey:@"date"] ];
		// NSDate로 된 키로 정렬
        //		NSDate *date1 = [(NSMutableDictionary *)obj1 objectForKey:@"date"];
        //		NSDate *date2 = [(NSMutableDictionary *)obj2 objectForKey:@"date"];
        //		if (date1==nil) return NSOrderedDescending; // 왼쪽게 더 오래된 거
        //		if (date2==nil) return NSOrderedAscending;
		NSNumber *date1 = [(NSMutableDictionary *)obj1 objectForKey:@"timeInterval"];
		NSNumber *date2 = [(NSMutableDictionary *)obj2 objectForKey:@"timeInterval"];
		return [date1 compare:date2];
	} ];
}
*/




CGFloat distanceBetweenPoint(CGPoint pt1, CGPoint pt2)
{
    CGFloat distance = sqrt( pow(pt1.x-pt2.x, 2)+pow(pt1.y-pt2.y, 2) );
    return distance;
}
+ (CGFloat)distanceBetweenPoint1:(CGPoint)pt1 point2:(CGPoint)pt2
{
    CGFloat distance = sqrt( pow(pt1.x-pt2.x, 2)+pow(pt1.y-pt2.y, 2) );
    return distance;
}




+ (NSString *)loadStringWithPathForResource:(NSString *)fileName ofType:(NSString *)extension
{
    if (extension==nil) {
        extension = @"txt";
    }
    NSString *filePath = [[NSBundle mainBundle] pathForResource:fileName ofType:extension];
    if (filePath) {
        return [[NSString alloc]initWithContentsOfFile:filePath encoding:NSUTF16StringEncoding error:nil];
    }
    return nil;
}

+(UIViewController*)topMostController {
    return [self topViewControllerWithRootViewController:[UIApplication sharedApplication].keyWindow.rootViewController];
}

+ (UIViewController*)topViewControllerWithRootViewController:(UIViewController*)rootViewController {
    if ([rootViewController isKindOfClass:[UITabBarController class]]) {
        UITabBarController* tabBarController = (UITabBarController*)rootViewController;
        return [self topViewControllerWithRootViewController:tabBarController.selectedViewController];
    } else if ([rootViewController isKindOfClass:[UINavigationController class]]) {
        UINavigationController* navigationController = (UINavigationController*)rootViewController;
        return [self topViewControllerWithRootViewController:navigationController.visibleViewController];
    } else if (rootViewController.presentedViewController) {
        UIViewController* presentedViewController = rootViewController.presentedViewController;
        return [self topViewControllerWithRootViewController:presentedViewController];
    } else {
        return rootViewController;
    }
}

+ (NSString *)getDeviceUUID
{
    NSUserDefaults* userDefault = [NSUserDefaults standardUserDefaults];
    NSString* iOSUUID = [userDefault stringForKey:@"UUID_KEY"];
    
    // new uuid
    if ( iOSUUID == nil || [iOSUUID  isEqual: @""] )
    {
        iOSUUID = [[NSUUID UUID] UUIDString];
    }
    
    // save uuid
    [userDefault setObject:iOSUUID forKey:@"UUID_KEY"];
    [userDefault synchronize];
    return iOSUUID;
}





//+(UIViewController *)topMostController{
//    UIViewController *topController = [[UIApplication sharedApplication] keyWindow].rootViewController;
//    while ([topController presentedViewController]) {
//        topController = [topController presentedViewController];
//    }
//    
//    if ([topController isKindOfClass:[UIAlertController class]]){
//        return [topController presentingViewController];
//    }
//    return topController;
//}

static char blockKeyImagePickerDidFinish;
static char blockKeyImagePickerDidCancel;
+ (UIImagePickerController *)presentImagePickerController:(UIViewController *)superViewController
                                                 animated:(BOOL)animated
                                        presentCompletion:(void(^)())presentCompletion
                                            allowsEditing:(BOOL)allowsEditing
                                               sourceType:(UIImagePickerControllerSourceType)sourceType
                                         didFinishPicking:(void(^)(UIImagePickerController *picker, NSDictionary *info, UIImage *image))didFinishPicking
                                                didCancel:(void(^)(UIImagePickerController *picker))didCancel
{
    UIImagePickerController *picker = nil;
    if([UIImagePickerController isSourceTypeAvailable:sourceType])
    {
        picker = [[UIImagePickerController alloc] init];
        picker.delegate = (id <UINavigationControllerDelegate, UIImagePickerControllerDelegate>)[self class];
        picker.allowsEditing = allowsEditing;
        picker.sourceType = sourceType;
        [superViewController presentViewController:picker animated:animated completion:presentCompletion];
        
        if (didFinishPicking) {
            objc_setAssociatedObject(picker, &blockKeyImagePickerDidFinish, didFinishPicking, OBJC_ASSOCIATION_COPY);
        }
        if (didCancel) {
            objc_setAssociatedObject(picker, &blockKeyImagePickerDidCancel, didCancel, OBJC_ASSOCIATION_COPY);
        }
    }
    return picker;
}
+ (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
	UIImage *editedImage = (UIImage*) [info valueForKey:UIImagePickerControllerEditedImage];
    if(!editedImage)
        editedImage = (UIImage*) [info valueForKey:UIImagePickerControllerOriginalImage];
    
    void(^didFinishBlock)(UIImagePickerController *picker, NSDictionary *info, UIImage *image) = objc_getAssociatedObject(picker, &blockKeyImagePickerDidFinish);
    if (didFinishBlock) {
        didFinishBlock(picker, info, editedImage);
    }
}
+ (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    void(^didCancelBlock)(UIImagePickerController *picker) = objc_getAssociatedObject(picker, &blockKeyImagePickerDidCancel);
    if (didCancelBlock) {
        didCancelBlock(picker);
    } else {
        [picker dismissViewControllerAnimated:YES completion:nil];
    }
}
// temp - 아래 UIImagePickerController 카테고리 참고
+ (void)navigationController:(UINavigationController *)navigationController didShowViewController:(UIViewController *)viewController animated:(BOOL)animated
{
    if ([navigationController.viewControllers count]==3)
    {
        UIImagePickerController *picker = (UIImagePickerController *)navigationController;
        CGFloat cropCircleRadius = [picker cropCircleRadius];
        if ( cropCircleRadius==0  ) {
            return;
        }
    
        CGFloat screenWidth = [[UIScreen mainScreen] bounds].size.width;
        CGFloat screenHeight = [[UIScreen mainScreen] bounds].size.height;
        
        UIView *plCropOverlay = [[[viewController.view.subviews objectAtIndex:1]subviews] objectAtIndex:0];
        plCropOverlay.hidden = YES;
        
        CAShapeLayer *circleLayer = [CAShapeLayer layer];
        
        UIBezierPath *path2 = nil;
        int position = 0;
        if (cropCircleRadius>0) {
            position = (screenHeight-cropCircleRadius*2)/2;
            path2 = [UIBezierPath bezierPathWithOvalInRect:
                     CGRectMake(screenWidth/2-cropCircleRadius, position, cropCircleRadius*2, cropCircleRadius*2)];
        }
        [path2 setUsesEvenOddFillRule:YES];
        
        [circleLayer setPath:[path2 CGPath]];
        [circleLayer setFillColor:[[UIColor clearColor] CGColor]];
        
        UIBezierPath *path = [UIBezierPath bezierPathWithRoundedRect:CGRectMake(0, 0, screenWidth, screenHeight-72) cornerRadius:0];
        [path appendPath:path2];
        [path setUsesEvenOddFillRule:YES];
        
        CAShapeLayer *fillLayer = [CAShapeLayer layer];
        fillLayer.path = path.CGPath;
        fillLayer.fillRule = kCAFillRuleEvenOdd;
        fillLayer.fillColor = [UIColor blackColor].CGColor;
        fillLayer.opacity = 0.8;
        [viewController.view.layer addSublayer:fillLayer];
        
        UILabel *moveLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 10, screenWidth, 50)];
        [moveLabel setText:@"Move and Scale"];
        [moveLabel setBackgroundColor:[UIColor clearColor]];
        [moveLabel setTextAlignment:NSTextAlignmentCenter];
        [moveLabel setTextColor:[UIColor whiteColor]];
        
        [viewController.view addSubview:moveLabel];
    }
}


+ (NSString *)bundleVersion
{
    return [[NSBundle mainBundle]objectForInfoDictionaryKey:@"CFBundleVersion"];
}

+ (NSString *)appleLanguage
{
    //        let localeCode = NSLocale.currentLocale().objectForKey(NSLocaleLanguageCode)!
    //        print(localeCode)
    //let pre = NSLocale.preferredLanguages()[0]
    
    //NSString* language = [[[NSUserDefaults standardUserDefaults] objectForKey:@"AppleLanguages"] objectAtIndex:0];
    NSString *language = [NSLocale preferredLanguages][0];
    if( [language rangeOfString:@"en"].location != NSNotFound ){
        //if( [language isEqualToString:@"en"] ){
        // english resource 사용
    } else if( [language rangeOfString:@"ko"].location != NSNotFound ){
        // korea resource 사용
    } else {
        // default resource
    }
    return language;
}



+ (void)runSpinAnimationOnView:(UIView*)view duration:(CGFloat)duration rotations:(CGFloat)rotations repeat:(float)repeat
{
    CABasicAnimation* rotationAnimation;
    rotationAnimation = [CABasicAnimation animationWithKeyPath:@"transform.rotation.z"];
    rotationAnimation.toValue = [NSNumber numberWithFloat: M_PI * 2.0 /* full rotation*/ * rotations * duration ];
    rotationAnimation.duration = duration;
    rotationAnimation.cumulative = YES;
    rotationAnimation.repeatCount = repeat;
    
    [view.layer addAnimation:rotationAnimation forKey:@"rotationAnimation"];
}



+ (BOOL)saveData:(NSData *)data toDocumentsDirectory:(NSString *)directoryPath
{
    NSString *savedDataPath = [Functions getFilePathFromDocumentsDirectory:directoryPath];

    // 기본적으로 오버라이트 지원 됨
//    if([[NSFileManager defaultManager] fileExistsAtPath:savedDataPath]) {
//        NSError *error;
//        [[NSFileManager defaultManager] removeItemAtPath:savedDataPath error:&error];
//    }
    BOOL isSucceed = [data writeToFile:savedDataPath atomically:NO];
    return isSucceed;
}
+ (NSString *)getFilePathFromDocumentsDirectory:(NSString *)directoryPath
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *savedDataPath = [documentsDirectory stringByAppendingPathComponent:directoryPath];
    return savedDataPath;
}
+ (void)removeDataAtDocumentsDirectoryPath:(NSString *)directoryPath
{
    if ([directoryPath length]==0) {
        return;
    }
    
    NSString *savedDataPath = [Functions getFilePathFromDocumentsDirectory:directoryPath];

    NSError *error;
    [[NSFileManager defaultManager]removeItemAtPath:savedDataPath error:&error];
    if (error)
    {
    }
}


+ (BOOL)isValidEmail:(NSString*)emailString
{
    if([emailString length]==0){
        return NO;
    }
    
    NSString *regExPattern = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    
    NSRegularExpression *regEx = [[NSRegularExpression alloc] initWithPattern:regExPattern
                                                                      options:NSRegularExpressionCaseInsensitive
                                                                        error:nil];
    NSUInteger regExMatches = [regEx numberOfMatchesInString:emailString options:0 range:NSMakeRange(0, [emailString length])];
    
    //NSLog(@"%lu", (unsigned long)regExMatches);
    if (regExMatches == 0)
    {
        return NO;
    }
    else
    {
        NSArray *components = [emailString componentsSeparatedByString:@"@"];
        if ([components count]!=2) {
            return NO;
        }
        
        return YES;
    }
}


+ (void)receiveAppInfoWithID:(NSString *)appStoreID callBack:(void(^)(NSDictionary *appData ,NSString *version))callBack
{
    NSLocale *locale = [NSLocale currentLocale];
    NSString *countryCode = [locale objectForKey: NSLocaleCountryCode];
//    NSString *country = [locale displayNameForKey: NSLocaleCountryCode value: countryCode];
    
    NSString *storeURLString = [NSString stringWithFormat:@"http://itunes.apple.com/lookup?id=%@&country=%@", appStoreID, countryCode];
    NSURL *storeURL = [NSURL URLWithString:storeURLString];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:storeURL];
    [request setHTTPMethod:@"GET"];
    
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    [NSURLConnection sendAsynchronousRequest:request queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
    {
        if ( [data length] > 0 && !error ) {
            NSDictionary *appData = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil];
            dispatch_async(dispatch_get_main_queue(), ^{
                NSArray *versionsInAppStore = [[appData valueForKey:@"results"] valueForKey:@"version"];
                if ( ![versionsInAppStore count] ) { // No versions of app in AppStore
                    callBack([NSDictionary dictionary], nil);
                } else {
                    callBack(appData, [versionsInAppStore firstObject]);
                }
            });
        } else {
            dispatch_async(dispatch_get_main_queue(), ^{
                callBack(nil, nil);
            });
        }
    }];
}

static char blockKeyStoreDidFinishBlock;
+ (void)presentAppStoreForID:(NSNumber *)appStoreID toSuperViewController:(UIViewController *)superViewController didFinish:(void(^)(UIViewController *viewController))didFinishBlock
//(void(^)(SKStoreProductViewController *viewController))didFinishBlock 원래는 이건데 이럴려면 @import StoreKit 헤더에 임포트해야함
{
    if (NSClassFromString(@"SKStoreProductViewController")) { // Checks for iOS 6 feature.
        
        SKStoreProductViewController *storeController = [[SKStoreProductViewController alloc] init];
        storeController.delegate = (id<SKStoreProductViewControllerDelegate>)[self class]; // productViewControllerDidFinish
        
        // Example App Store ID (e.g. for Words With Friends)
        // @322852954
        
        [storeController loadProductWithParameters:@{ SKStoreProductParameterITunesItemIdentifier: appStoreID }
                                   completionBlock:^(BOOL result, NSError *error) {
                                       if (result) {
                                           [superViewController presentViewController:storeController animated:YES completion:nil];
                                       } else {
                                           [[[UIAlertView alloc] initWithTitle:@"Uh oh!" message:@"There was a problem opening the app store" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil] show];
                                       }
                                   }];
        
        objc_setAssociatedObject(storeController, &blockKeyStoreDidFinishBlock, didFinishBlock, OBJC_ASSOCIATION_COPY);
        
    } else { // Before iOS 6, we can only open the App Store URL
        NSString *iTunesLink = [NSString stringWithFormat:@"itms-apps://phobos.apple.com/WebObjects/MZStore.woa/wa/viewSoftwareUpdate?id=%@&mt=8", appStoreID];
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:iTunesLink]];
    }
}
+ (void)productViewControllerDidFinish:(SKStoreProductViewController *)viewController
{
    // alerView delegate
    void(^didFinishBlock)(SKStoreProductViewController *viewController) = objc_getAssociatedObject(viewController, &blockKeyStoreDidFinishBlock);
    if (didFinishBlock) {
        didFinishBlock(viewController);
    } else {
        [viewController dismissViewControllerAnimated:YES completion:nil];
    }
}




+ (void)showAlertWithTitle:(NSString *)title message:(NSString *)message buttonTitles:(NSArray *)buttons delegate:(void(^)(NSInteger buttonIndex))delegate
{
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0"))
    {
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
        
        for (int i=0; i<[buttons count]; i++) {
            NSString *buttonTitle = [buttons objectAtIndex:i];
            UIAlertAction *alertAction = [UIAlertAction actionWithTitle:buttonTitle style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                if (delegate) {
                    delegate(i);
                }
                [alertController dismissViewControllerAnimated:YES completion:nil];
            }];
            [alertController addAction:alertAction];
        }
        
        UIViewController *superViewController = [[[UIApplication sharedApplication] delegate] window].rootViewController;
        while (superViewController.presentedViewController!=nil) {
            superViewController = superViewController.presentedViewController;
        }
        [superViewController presentViewController:alertController animated:YES completion:nil];
    }
    else
    {
        return [UIAlertView showWithTitle:title message:message buttonTitles:buttons delegate:delegate];
    }
}
+ (void)showAlertWithTitle:(NSString *)title message:(NSString *)message
{
    return [Functions showAlertWithTitle:title message:message buttonTitles:@[ALERT_DEFAULT_BUTTON_OK] delegate:nil];
}
+ (void)showAlertNetwordErrorMessage
{
    return [Functions showAlertWithTitle:ALERT_TITLE_NETWORK_ERROR message:ALERT_MESSAGE_NETWORK_ERROR];
}
+ (BOOL)doesAlertExistWithTitle:(NSString *)title superViewController:(UIViewController *)superViewController
{
    if ( SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0") )
    {
        // UIAlertController가 붙어있을 때 또 붙일 경우 자동으로 처리가 됨
        return NO;
    }
    else if ( SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0") )
    {
        // 알 수 있는 방법 없음
        // temp - private API
        //        UIAlertView *alert = [NSClassFromString(@"_UIAlertManager") performSelector:@selector(topMostAlert)];
        //        if ([alert.title isEqualToString:title]) {
        //            return YES;
        //        }
        return NO;
    }
    else
    {
        for (UIWindow *window in [UIApplication sharedApplication].windows) {
            NSArray *subviews = window.subviews;
            if ([subviews count] > 0) {
                id subview = [subviews objectAtIndex:0];
                if ([subview isKindOfClass:[UIAlertView class]] || [subview isKindOfClass:[UIActionSheet class]]) {
                    if ([[subview title] isEqualToString:title])
                        return YES;
                }
            }
        }
    }
    return NO;
}


+ (id)presentActionSheetWithTitle:(NSString *)title
                          message:(NSString *)message
                cancelButtonTitle:(NSString *)cancelButtonTitle
           destructiveButtonTitle:(NSString *)destructiveButtonTitle
                otherButtonTitles:(NSArray *)otherButtonTitles
                         delegate:(void(^)(UIAlertController *alertController, UIAlertAction *action, NSInteger buttonIndex))delegate
{
    return [Functions presentActionSheetInViewController:nil withTitle:title message:message cancelButtonTitle:cancelButtonTitle destructiveButtonTitle:destructiveButtonTitle otherButtonTitles:otherButtonTitles delegate:delegate];
}
+ (id)presentActionSheetInViewController:(UIViewController *)viewController
                               withTitle:(NSString *)title
                                 message:(NSString *)message
                       cancelButtonTitle:(NSString *)cancelButtonTitle
                  destructiveButtonTitle:(NSString *)destructiveButtonTitle
                       otherButtonTitles:(NSArray *)otherButtonTitles
                                delegate:(void(^)(UIAlertController *alertController, UIAlertAction *action, NSInteger buttonIndex))delegate
{
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0"))
    {
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleActionSheet];
        [alertController dismissViewControllerAnimated:YES completion:nil];

        UIAlertAction *action;
        
        if (cancelButtonTitle) {
            action = [UIAlertAction actionWithTitle:cancelButtonTitle style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
                if (delegate) {
                    delegate(alertController, action, UIActionSheetCancelButtonIndex);
                }
            }];
            [alertController addAction:action];
        }

        if (destructiveButtonTitle) {
            action = [UIAlertAction actionWithTitle:destructiveButtonTitle style:UIAlertActionStyleDestructive handler:^(UIAlertAction *action) {
                if (delegate) {
                    delegate(alertController, action, UIActionSheetDestructiveButtonIndex);
                }
            }];
            [alertController addAction:action];
        }
        
        for (int i=0; i<[otherButtonTitles count]; i++) {
            NSString *buttonTitle = [otherButtonTitles objectAtIndex:i];
            action = [UIAlertAction actionWithTitle:buttonTitle style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                if (delegate) {
                    delegate(alertController, action, UIActionSheetFirstOtherButtonIndex+i);
                }
            }];
            [alertController addAction:action];
        }

        //[alertController addAction:];
        
        if (viewController==nil) {
            UIViewController *superViewController = [[[UIApplication sharedApplication] delegate] window].rootViewController;
            if (superViewController.presentedViewController) {
                superViewController = superViewController.presentedViewController;
            }
            viewController = superViewController;
        }
        [viewController presentViewController:alertController animated:YES completion:nil];
        return alertController;
    }
    else
    {
        UIActionSheet *actionSheet = [[UIActionSheet alloc]initWithTitle:title delegate:(id<UIActionSheetDelegate>)self cancelButtonTitle:cancelButtonTitle destructiveButtonTitle:destructiveButtonTitle otherButtonTitles:nil];
        for (int i=0; i<[otherButtonTitles count]; i++) {
            NSString *buttonTitle = [otherButtonTitles objectAtIndex:i];
            [actionSheet addButtonWithTitle:buttonTitle];
        }
        objc_setAssociatedObject(actionSheet, &blockKeyActionSheet, delegate, OBJC_ASSOCIATION_RETAIN);
        return actionSheet;
    }
}

static char blockKeyActionSheet;
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    // alerView delegate
    void(^delegateBlock)(UIAlertController *alertController, UIAlertAction *action, NSInteger buttonIndex) = objc_getAssociatedObject(actionSheet, &blockKeyActionSheet);
    if (delegateBlock) {
        delegateBlock(nil, nil, buttonIndex);
    }
}

@end




// -------------------------------------------------- Category .m -------------------------------------------------- //
//

@implementation NSObject (PerformBlockAfterDelay)
- (void)performBlock:(void (^)(void))block
          afterDelay:(NSTimeInterval)delay
{
//    [self performSelector:@selector(fireBlockAfterDelay:)
//               withObject:block
//               afterDelay:delay];
    
    double delayInSeconds = delay;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), block);
}
//- (void)fireBlockAfterDelay:(void (^)(void))block
//{
//    block();
//}

- (id)nilIfNSNull
{
    return ([self isEqual:[NSNull null]]? nil : self);
}

@end







@implementation UIAlertView (UIAlertViewCategory)
static char blockKey;

+ (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    // alerView delegate
    void(^delegateBlock)(NSInteger buttonIndex) = objc_getAssociatedObject(alertView, &blockKey);
    if (delegateBlock) {
        delegateBlock(buttonIndex);
    }
}
+ (void)showWithTitle:(NSString *)title message:(NSString *)message buttonTitles:(NSArray *)buttons delegate:(void(^)(NSInteger buttonIndex))delegate
{
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:title
                                                   message:message
                                                  delegate:[self class]
                                         cancelButtonTitle:nil
                                         otherButtonTitles:nil];
    for (int i=0; i<[buttons count]; i++)
    {
        [alert addButtonWithTitle: [buttons objectAtIndex:i] ];
    }
    if (buttons==nil) {
        [alert addButtonWithTitle: ALERT_DEFAULT_BUTTON_OK ];
    }
	[alert performSelectorOnMainThread:@selector(show) withObject:nil waitUntilDone:NO];
    
    objc_setAssociatedObject(alert, &blockKey, delegate, OBJC_ASSOCIATION_COPY);
}

+ (void)showWithTitle:(NSString *)messageTitle message:(NSString *)message
{
	UIAlertView *alert = [[UIAlertView alloc]initWithTitle:messageTitle message:message delegate:self cancelButtonTitle:ALERT_DEFAULT_BUTTON_OK otherButtonTitles:nil];
	[alert performSelectorOnMainThread:@selector(show) withObject:nil waitUntilDone:NO];
}
+ (void)showNetwordErrorMessage
{
    if ([UIAlertView doesAlertViewExistWithTitle:ALERT_TITLE_NETWORK_ERROR]==NO) {
        [UIAlertView showWithTitle:ALERT_TITLE_NETWORK_ERROR
                           message:ALERT_MESSAGE_NETWORK_ERROR];
    }
}

+ (id)showAlertViewWithLoadingAnimationWithTitle:(NSString *)title
{
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:title message:nil delegate:nil cancelButtonTitle:nil otherButtonTitles:nil];
    //
    UIActivityIndicatorView *loading = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
    [alert addSubview:loading];
    
    //    runOnMainQueueWithoutDeadlocking(^{
    [alert show];

    CGPoint loadingCenter = CGPointMake(alert.viewForBaselineLayout.bounds.size.width/2,
                                        alert.viewForBaselineLayout.bounds.size.height - 50);
    
    [loading startAnimating];
    [loading setCenter:loadingCenter];
    //    });
    [alert performSelectorOnMainThread:@selector(show) withObject:nil waitUntilDone:YES];
    [loading performSelectorOnMainThread:@selector(startAnimating) withObject:nil waitUntilDone:YES];
    
    return alert;
}
+ (void)dismissAlertViewWithLoadingAnimation
{
    for (UIWindow *window in [UIApplication sharedApplication].windows) {
        for (UIView *view in window.subviews) {
            BOOL alert = [view isKindOfClass:[UIAlertView class]];
            BOOL action = [view isKindOfClass:[UIActionSheet class]];
            if ((alert || action) && [(UIAlertView *)view buttonTitleAtIndex:0]==nil) {
                //                runOnMainQueueWithoutDeadlocking(^{
                [(UIAlertView *)view dismissWithClickedButtonIndex:0 animated:YES];
                //                });
            }
        }
    }
}




+ (BOOL)doesAlertViewExist
{
    if ( SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0") )
    {
        // temp - private API
//        UIAlertView *alert = [NSClassFromString(@"_UIAlertManager") performSelector:@selector(topMostAlert)];
//        if (alert) {
//            return YES;
//        }
        return NO;
    }
    else
    {
        for (UIWindow *window in [UIApplication sharedApplication].windows) {
            for (UIView *view in window.subviews) {
                BOOL alert = [view isKindOfClass:[UIAlertView class]];
                BOOL action = [view isKindOfClass:[UIActionSheet class]];
                if (alert || action)
                    return YES;
            }
        }
    }
    return NO;
}
+ (BOOL)doesAlertViewExistWithTitle:(NSString *)title
{
    if ( SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0") )
    {
        // temp - private API
//        UIAlertView *alert = [NSClassFromString(@"_UIAlertManager") performSelector:@selector(topMostAlert)];
//        if ([alert.title isEqualToString:title]) {
//            return YES;
//        }
        return NO;
    }
    else
    {
        for (UIWindow *window in [UIApplication sharedApplication].windows) {
            NSArray *subviews = window.subviews;
            if ([subviews count] > 0) {
                id subview = [subviews objectAtIndex:0];
                if ([subview isKindOfClass:[UIAlertView class]] || [subview isKindOfClass:[UIActionSheet class]]) {
                    if ([[subview title] isEqualToString:title])
                        return YES;
                }
            }
        }
    }
    return NO;
}

@end





@implementation NSString (NSStringCategory)

- (NSDate *)dateWithDateFormat:(NSString *)dateFormat
{
    // dateFormat 형식의 길이와 string의 길이 형식이 일치해야함
	NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
    if (dateFormat==nil) {
        [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    } else {
        [dateFormatter setDateFormat:dateFormat];
    }
	NSDate *dateFromString = [dateFormatter dateFromString:self];
	
	return dateFromString;
}

- (NSString *)moneyFormat
{
    int stringLength = (int)[self length];
    int lengthOfDividedBundle = 3;
    if (stringLength<=lengthOfDividedBundle) {
        return self;
    }
    
    NSMutableArray *stringBundleArray = [NSMutableArray array];
    int numberOfComma = (stringLength-1)/lengthOfDividedBundle;
    for (int i=0; i<numberOfComma+1; i++) {
        int startIndex = stringLength - (i+1)*lengthOfDividedBundle;
        if (startIndex<0) {
            lengthOfDividedBundle += startIndex;
            startIndex = 0;
        }
        NSRange bundleRange = NSMakeRange(startIndex, lengthOfDividedBundle);
        NSString *stringBundle = [self substringWithRange:bundleRange];
        [stringBundleArray insertObject:stringBundle atIndex:0];
    }
    
    NSString *transformedString = [stringBundleArray componentsJoinedByString:@","];
    // 마이너스인 경우, 예)-,500원
    transformedString = [transformedString stringByReplacingOccurrencesOfString:@"-," withString:@"-"];
    
    
    return transformedString;
}
- (NSString *)restoreMoneyFormat;
{
    NSString *resultString = [self stringByReplacingOccurrencesOfString:@"," withString:@""];
//    resultString = [self stringByReplacingOccurrencesOfString:@"원" withString:@""];
    return resultString;
}
- (int)restoreMoneyFormatToInt
{
    NSString *unitIfExist = NSLocalizedString(@"원", nil);
    NSString *resultString = [[self restoreMoneyFormat] stringByReplacingOccurrencesOfString:unitIfExist withString:@""];
    return [resultString intValue];
}

- (NSString *)URLEncodedString_ch
{
    NSMutableString * output = [NSMutableString string];
    const unsigned char * source = (const unsigned char *)[self UTF8String];
    int sourceLen = (int)strlen((const char *)source);
    for (int i = 0; i < sourceLen; ++i) {
        const unsigned char thisChar = source[i];
        if (thisChar == ' '){
            [output appendString:@"+"];
        } else if (thisChar == '.' || thisChar == '-' || thisChar == '_' || thisChar == '~' ||
                   (thisChar >= 'a' && thisChar <= 'z') ||
                   (thisChar >= 'A' && thisChar <= 'Z') ||
                   (thisChar >= '0' && thisChar <= '9')) {
            [output appendFormat:@"%c", thisChar];
        } else {
            [output appendFormat:@"%%%02X", thisChar];
        }
    }
    return output;
}

- (NSNumber *)NSNumberValue
{
    NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
    [numberFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
    NSNumber *NSNumberValue = [numberFormatter numberFromString:self];
    return NSNumberValue;
}

@end





@implementation NSDate (NSDateCategory)

- (NSString *)monthName
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat: @"MMMM"];
        dateFormatter.locale = [[NSLocale alloc] initWithLocaleIdentifier: [Functions appleLanguage] ];
    NSString *weekdayName = [dateFormatter stringFromDate:self];
    return weekdayName;
    //    NSDateComponents *components = [[NSCalendar currentCalendar] components:NSWeekdayCalendarUnit fromDate:self];
    //    return [components weekday];
}
- (NSString *)weekdayName
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
//    [dateFormatter setDateFormat: @"EEEE"];
    [dateFormatter setDateFormat: @"EEEE"];
    dateFormatter.locale = [[NSLocale alloc] initWithLocaleIdentifier: [Functions appleLanguage] ];
    NSString *weekdayName = [dateFormatter stringFromDate:self];
    return weekdayName;
//    NSDateComponents *components = [[NSCalendar currentCalendar] components:NSWeekdayCalendarUnit fromDate:self];
//    return [components weekday];
}
- (NSInteger)year
{
    NSDateComponents *components = nil;
//    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
        components = [[NSCalendar currentCalendar] components:NSCalendarUnitYear fromDate:self];
//    } else {
//        components = [[NSCalendar currentCalendar] components:NSYearCalendarUnit fromDate:self];
//    }
    return [components year];
}
- (NSInteger)month
{
    NSDateComponents *components = nil;
//    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
        components = [[NSCalendar currentCalendar] components:NSCalendarUnitMonth fromDate:self];
//    } else {
//        components = [[NSCalendar currentCalendar] components:NSMonthCalendarUnit fromDate:self];
//    }
    return [components month];
}
- (NSInteger)day
{
    NSDateComponents *components = nil;
//    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
        components = [[NSCalendar currentCalendar] components:NSCalendarUnitDay fromDate:self];
//    } else {
//        components = [[NSCalendar currentCalendar] components:NSDayCalendarUnit fromDate:self];
//    }
    return [components day];
}
- (NSString *)stringWithDateFormat:(NSString *)dateFormat
{
    if (dateFormat==nil) {
        dateFormat = @"yyyy-MM-dd HH:mm:ss";
    }
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:dateFormat];
    NSString *stringDate = [dateFormatter stringFromDate:self];
    
    return stringDate;
}

+ (NSDate *)localDateTime
{
    NSDate* sourceDate = [NSDate date];
    
    NSTimeZone *sourceTimeZone = [NSTimeZone timeZoneWithAbbreviation:@"GMT"];
    NSTimeZone *destinationTimeZone = [NSTimeZone systemTimeZone];
    
    NSInteger sourceGMTOffset = [sourceTimeZone secondsFromGMTForDate:sourceDate];
    NSInteger destinationGMTOffset = [destinationTimeZone secondsFromGMTForDate:sourceDate];
    NSTimeInterval interval = destinationGMTOffset - sourceGMTOffset;
    
    NSDate *destinationDate = [[NSDate alloc] initWithTimeInterval:interval sinceDate:sourceDate];

    return destinationDate;
}
+ (NSDate *)dateWithDay:(NSInteger)day month:(NSInteger)month year:(NSInteger)year
{
    NSDateComponents *components = [[NSDateComponents alloc] init];
    components.year = year;
    components.month = month;
    components.day = day;
    return [[NSCalendar currentCalendar] dateFromComponents:components];
}

@end



@implementation UIView (UIViewCategory)

- (UITableViewCell *)getTableViewCell
{
    UIView *view = self;
    if ([view isKindOfClass:[UITableViewCell class]]) {
        return (UITableViewCell *)view;
    }

    while ((view = view.superview)) {
        if ([view isKindOfClass:[UITableViewCell class]]) {
            return (UITableViewCell *)view;
        }
    }
    return nil;
}
- (UICollectionViewCell *)getCollectionViewCell
{
    UIView *view = self;
    if ([view isKindOfClass:[UICollectionViewCell class]]) {
        return (UICollectionViewCell *)view;
    }
    
    while ((view = view.superview)) {
        if ([view isKindOfClass:[UICollectionViewCell class]]) {
            return (UICollectionViewCell *)view;
        }
    }
    return nil;
}

- (UIViewController *)viewController;
{
    id nextResponder = [self nextResponder];
    if ([nextResponder isKindOfClass:[UIViewController class]]) {
        return nextResponder;
    } else {
        return nil;
    }
}

#define kLoading -424
- (UIActivityIndicatorView *)getActivityIndicatorView
{
    UIActivityIndicatorView *loading = nil;
    for (id subview in self.subviews) {
        if ([subview isKindOfClass:[UIActivityIndicatorView class]]) {
            if ([subview tag]==kLoading) {
                loading = subview;
                break;
            }
        }
    }
    return loading;
}
- (UIActivityIndicatorView *)startLoadingAnimationOnCenter:(CGPoint)center
{
    UIActivityIndicatorView *loading = [self getActivityIndicatorView];
	if (loading==nil) {
		loading = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
		[loading setTag:kLoading];
		[self addSubview:loading];
	}
    [loading setCenter:center];
	[loading startAnimating];
    return loading;
}
- (void)stopLoadingAnimation
{
	UIActivityIndicatorView *loading = (UIActivityIndicatorView *)[self viewWithTag:kLoading];
	[loading stopAnimating];
}

static char blockKeyTapDelegate;
- (UITapGestureRecognizer *)setTapDelegate:(void(^)(UITapGestureRecognizer *tapGestureRecognizer))delegateBlock
{
    if (delegateBlock==nil) {
//      #2 아래처럼 할 경우 다른 곳에서 추가한 제스처가 제거될 수 있음
//        for (UIGestureRecognizer *gesture in [self gestureRecognizers]) {
//            [self removeGestureRecognizer:gesture];
//        }
        for (UIGestureRecognizer *gesture in [self gestureRecognizers]) {
            if ([gesture isKindOfClass:[UITapGestureRecognizer class]] && [gesture.view isEqual:self]) {
                if ([(UITapGestureRecognizer*)gesture numberOfTapsRequired]==1) {
                    [self removeGestureRecognizer:gesture];
                }
            }
        }
        return nil;
    }
    
    [self setUserInteractionEnabled:YES];
    
    UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(didTappedInCategory:)];
    tapGestureRecognizer.numberOfTapsRequired = 1;
    [self addGestureRecognizer:tapGestureRecognizer];
    
    objc_setAssociatedObject(self, &blockKeyTapDelegate, delegateBlock, OBJC_ASSOCIATION_COPY);
    
    return tapGestureRecognizer;
}
- (void)didTappedInCategory:(UITapGestureRecognizer *)sender
{
    void (^delegateBlock)(UITapGestureRecognizer *tapGestureRecognizer) = objc_getAssociatedObject(self, &blockKeyTapDelegate);
    if (delegateBlock) {
        delegateBlock(sender);
    }
}

static char blockKeyLongPressDelegate;
- (void)setLongPressDelegate:(void(^)(UILongPressGestureRecognizer *gestureRecognizer))delegateBlock minimumPressDuration:(CFTimeInterval)minimumPressDuration;
{
    if (delegateBlock==nil) {
        for (UIGestureRecognizer *gesture in [self gestureRecognizers]) {
            if ([gesture isKindOfClass:[UILongPressGestureRecognizer class]] && [gesture.view isEqual:self]) {
                [self removeGestureRecognizer:gesture];
            }
        }
        return;
    }

    [self setUserInteractionEnabled:YES];
    
    UILongPressGestureRecognizer *gestureRecognizer = [[UILongPressGestureRecognizer alloc]initWithTarget:self action:@selector(didLongPressInCategory:)];
    if (minimumPressDuration) {
        gestureRecognizer.minimumPressDuration = minimumPressDuration;
    }
    [self addGestureRecognizer:gestureRecognizer];
    
    objc_setAssociatedObject(self, &blockKeyLongPressDelegate, delegateBlock, OBJC_ASSOCIATION_COPY);
}
- (void)didLongPressInCategory:(UILongPressGestureRecognizer *)sender
{
    void (^delegateBlock)(UILongPressGestureRecognizer *gestureRecognizer) = objc_getAssociatedObject(self, &blockKeyLongPressDelegate);
    if (delegateBlock) {
        delegateBlock(sender);
    }
}


static char blockKeyDoubleTapDelegate;
- (UITapGestureRecognizer *)setDoubleTapDelegate:(void(^)(UITapGestureRecognizer *tapGestureRecognizer))delegateBlock
{
    if (delegateBlock==nil) {
        //      #2 아래처럼 할 경우 다른 곳에서 추가한 제스처가 제거될 수 있음
        //        for (UIGestureRecognizer *gesture in [self gestureRecognizers]) {
        //            [self removeGestureRecognizer:gesture];
        //        }
        for (UIGestureRecognizer *gesture in [self gestureRecognizers]) {
            if ([gesture isKindOfClass:[UITapGestureRecognizer class]] && [gesture.view isEqual:self]) {
                if ([(UITapGestureRecognizer*)gesture numberOfTapsRequired]==2) {
                    [self removeGestureRecognizer:gesture];
                }
            }
        }
        return nil;
    }
    
    [self setUserInteractionEnabled:YES];
    
    UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(didDoubleTappedInCategory:)];
    tapGestureRecognizer.numberOfTapsRequired = 2;
    [self addGestureRecognizer:tapGestureRecognizer];
    
    objc_setAssociatedObject(self, &blockKeyDoubleTapDelegate, delegateBlock, OBJC_ASSOCIATION_COPY);
    
    return tapGestureRecognizer;
}
- (void)didDoubleTappedInCategory:(UITapGestureRecognizer *)sender
{
    void (^delegateBlock)(UITapGestureRecognizer *tapGestureRecognizer) = objc_getAssociatedObject(self, &blockKeyDoubleTapDelegate);
    if (delegateBlock) {
        delegateBlock(sender);
    }
}


- (CGPoint)centerFromOrigin
{
    return CGPointMake(self.frame.size.width/2, self.frame.size.height/2);
}




- (void)setFrameX:(CGFloat)x
{
    CGRect frame = self.frame;
    frame.origin.x = x;
    self.frame = frame;
}
- (void)setFrameY:(CGFloat)y
{
    CGRect frame = self.frame;
    frame.origin.y = y;
    self.frame = frame;
}
- (void)setFrameWidth:(CGFloat)width
{
    CGRect frame = self.frame;
    frame.size.width = width;
    self.frame = frame;
}
- (void)setFrameHeight:(CGFloat)height
{
    CGRect frame = self.frame;
    frame.size.height = height;
    self.frame = frame;
}

- (void)setFrameOrigin:(CGPoint)origin
{
    CGRect frame = self.frame;
    frame.origin.x = origin.x;
    frame.origin.y = origin.y;
    self.frame = frame;
}
- (void)setFrameSize:(CGSize)size
{
    CGRect frame = self.frame;
    frame.size.width = size.width;
    frame.size.height = size.height;
    self.frame = frame;
}

- (void)removeSubviews
{
    [[self subviews] makeObjectsPerformSelector:@selector(removeFromSuperview)];
//    for (int i=[self.subviews count]-1; i>=0; i--) {
//        id subview = [self.subviews objectAtIndex:i];
//        [subview removeFromSuperview];
//    }
}

- (void)setDraggable
{
    UIPanGestureRecognizer *panRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(move:)];
    [panRecognizer setMinimumNumberOfTouches:1];
    [panRecognizer setMaximumNumberOfTouches:1];
    [panRecognizer setDelegate:(id<UIGestureRecognizerDelegate>)self];
    [self addGestureRecognizer:panRecognizer];
}
- (void)move:(UIPanGestureRecognizer *)gestureRecognizer
{
    UIView *view = gestureRecognizer.view;
    CGPoint translatedPoint = [(UIPanGestureRecognizer*)gestureRecognizer translationInView:view];
    
    static CGPoint center;
    if ([(UIPanGestureRecognizer*)gestureRecognizer state] == UIGestureRecognizerStateBegan) {
        center = [view center];
    }
        
    translatedPoint = CGPointMake(center.x+translatedPoint.x, center.y+translatedPoint.y);
    view.center = translatedPoint;
}

@end





@implementation UIImage (UIImageCategory)
/// 이미지 리사이즈시 회전 정보 보장되지 않음
- (UIImage*)resizeWithSize:(CGSize)size
{
	CGRect thumbRect = CGRectMake(0, 0, size.width, size.height);
	
	// Creates a bitmap-based graphics context and makes it the current context.
	//UIGraphicsBeginImageContext(size);
    UIGraphicsBeginImageContextWithOptions(size, NO, self.scale);
	[self drawInRect:thumbRect];
	UIImage *outImage = UIGraphicsGetImageFromCurrentImageContext();
	UIGraphicsEndImageContext();
    
//    outImage = [UIImage imageWithCGImage:outImage.CGImage
//                                   scale:self.scale
//                             orientation:self.imageOrientation];
	return outImage;
}
- (UIImage *)resizeWithScale:(CGFloat)scale
{
    return [self resizeWithSize:CGSizeMake(self.size.width*scale, self.size.height*scale)];
}
- (UIImage *)resizeFitToWidth:(CGFloat)width
{
    CGFloat adjustHeight = self.size.height*(width/self.size.width);
    return [self resizeWithSize:CGSizeMake(width, adjustHeight)];
}
- (UIImage *)resizeFitToHeight:(CGFloat)height
{
    CGFloat adjustWidth = self.size.width*(height/self.size.height);
    return [self resizeWithSize:CGSizeMake(adjustWidth, height)];
}

- (UIImage *)gaussianBlurWithRadius:(CGFloat)inputRadius
{
    // create our blurred image
    CIContext *context = [CIContext contextWithOptions:nil];
    CIImage *inputImage = [CIImage imageWithCGImage:self.CGImage];

    // setting up Gaussian Blur (we could use one of many filters offered by Core Image)
    CIFilter *filter = [CIFilter filterWithName:@"CIGaussianBlur"];
    [filter setValue:inputImage forKey:kCIInputImageKey];
    [filter setValue:[NSNumber numberWithFloat:inputRadius] forKey:@"inputRadius"];
    CIImage *result = [filter valueForKey:kCIOutputImageKey];
    
    // CIGaussianBlur has a tendency to shrink the image a little,
    // this ensures it matches up exactly to the bounds of our original image
    CGImageRef cgImage = [context createCGImage:result fromRect:[inputImage extent]];
//    UIImage *blurredImage = [UIImage imageWithCGImage:cgImage];
    
    // 회전방향 정보 추가
    UIImage *blurredImage = [[UIImage alloc] initWithCGImage:cgImage scale:1.0 orientation:[self imageOrientation]];
    CFRelease(cgImage);
    
    return blurredImage;
    
    // if you need scaling
    // return [[self class] scaleIfNeeded:cgImage];
}
@end




@implementation UITextView (UITextViewCategory)
- (void)setPaddingZero
{
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
        self.textContainer.lineFragmentPadding = 0;
        self.textContainerInset = UIEdgeInsetsZero;
    } else {
        self.contentInset = UIEdgeInsetsMake(-4,-8,0,0);
    }
}
@end




@implementation UILabel (UILabelCategory)
- (UILabel *)copyWithFrame:(CGRect)rect
{
    UILabel *original = self;
    
    UILabel *copyObject = [[UILabel alloc]initWithFrame:rect];
    if (original.backgroundColor) {
        copyObject.backgroundColor = original.backgroundColor;
    } else {
        copyObject.backgroundColor = [UIColor clearColor];
    }
    copyObject.font = original.font;
    copyObject.text = original.text;
    copyObject.textAlignment = original.textAlignment;
    copyObject.textColor = original.textColor;
    copyObject.shadowColor = original.shadowColor;
    copyObject.shadowOffset = original.shadowOffset;
    copyObject.lineBreakMode =  original.lineBreakMode;
    copyObject.numberOfLines = original.numberOfLines;
    copyObject.userInteractionEnabled = original.userInteractionEnabled;
    copyObject.highlightedTextColor = original.highlightedTextColor;
    copyObject.minimumScaleFactor = original.minimumScaleFactor;
    copyObject.adjustsFontSizeToFitWidth = original.adjustsFontSizeToFitWidth;
    //
    copyObject.alpha = original.alpha;
    copyObject.hidden = original.hidden;
    copyObject.tag = original.tag;
    //
    copyObject.enabled = original.enabled;
    copyObject.baselineAdjustment = original.baselineAdjustment;
    copyObject.highlighted = original.highlighted;
    
    return copyObject;
}
- (void)setNumberWithCountingAnimation:(NSString *)numberString changeInterval:(NSTimeInterval)changeInterval numberOfChange:(NSInteger)numberOfChange
{
    UILabel *label = self;
    int length = (int)[numberString length];
    
    for (int i=0; i<length; i++)
    {
        NSString *currentText = [numberString substringWithRange:NSMakeRange(length-i, i)];
        NSString *forSet = [numberString substringWithRange:NSMakeRange(length-1-i, 1)];
        
        for (int j=0; j<numberOfChange; j++)
        {
            NSTimeInterval afterDelay = changeInterval*numberOfChange*i+changeInterval*j;
            if (j==numberOfChange-1) {
                [label performSelector:@selector(setText:)
                            withObject:[NSString stringWithFormat:@"%@%@", forSet, currentText]
                            afterDelay:afterDelay];
            } else {
                [label performSelector:@selector(setText:)
                            withObject:[NSString stringWithFormat:@"%d%@", j%10, currentText]
                            afterDelay:afterDelay];
            }
        }
    }
}
- (void)setNumberWithCountingAnimation:(NSString *)numberString
{
    [self setNumberWithCountingAnimation:numberString changeInterval:0.05 numberOfChange:10];
}
- (void)cancelCountingAnimation
{
    [NSObject cancelPreviousPerformRequestsWithTarget:self];
}

- (void)setText:(NSString *)text withSwitchAnimationColor:(UIColor *)textColor
{
    if (text==nil) {
        return;
    }

    CGFloat scale = 0.001;
    [UIView animateWithDuration:0.2 animations:^{
        self.transform = CGAffineTransformScale(self.transform, scale, 1);
    } completion:^(BOOL finished) {
        self.text = text;
        self.textColor = textColor;
        [UIView animateWithDuration:0.2 animations:^{
            self.transform = CGAffineTransformMakeScale(1, 1);
        } completion:^(BOOL finished) {

        }];
    }];
}
@end




@implementation UIViewController (UIViewControllerCategory)
@end


/*
// temp - 이제 없애도 되나?
@implementation UINavigationController (UINavigationControllerCategory)
- (BOOL)shouldAutorotate
{
    return [[self.viewControllers lastObject] shouldAutorotate];
}
- (NSUInteger)supportedInterfaceOrientations
{
    return [[self.viewControllers lastObject] supportedInterfaceOrientations];
}
- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation
{
    return [[self.viewControllers lastObject] preferredInterfaceOrientationForPresentation];
}
@end





@implementation UITabBarController (UITabBarControllerCategory)
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // You do not need this method if you are not supporting earlier iOS Versions
    return [self.selectedViewController shouldAutorotateToInterfaceOrientation:interfaceOrientation];
}
- (BOOL)shouldAutorotate
{
//    [(AppDelegate*)[[UIApplication sharedApplication] delegate] layoutTabBarButton ];
    return [self.selectedViewController shouldAutorotate];
}
- (NSUInteger)supportedInterfaceOrientations
{
    return [self.selectedViewController supportedInterfaceOrientations];
}
- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation
{
    return [[self.viewControllers lastObject] preferredInterfaceOrientationForPresentation];
}
@end
*/



static char blockKeyCropRectSize;
static char blockKeyCropCircleRadius;
@implementation UIImagePickerController (NonRotating)
- (BOOL)shouldAutorotate
{
    return NO;
}
-(UIInterfaceOrientation)preferredInterfaceOrientationForPresentation
{
    return UIInterfaceOrientationPortrait;
}


- (void)setCropRectSize:(CGSize)cropRectSize
{
    objc_setAssociatedObject(self, &blockKeyCropRectSize, [NSValue valueWithCGSize:cropRectSize], OBJC_ASSOCIATION_RETAIN);
}
- (void)setCropCircleRadius:(CGFloat)circleRadius
{
    objc_setAssociatedObject(self, &blockKeyCropCircleRadius, [NSNumber numberWithFloat:circleRadius], OBJC_ASSOCIATION_RETAIN);
}
- (CGSize)cropRectSize
{
    NSValue *circleRadiusValue = objc_getAssociatedObject(self, &blockKeyCropRectSize);
    if (circleRadiusValue==nil) {
        return CGSizeZero;
    } else {
        return [circleRadiusValue CGSizeValue];
    }
}
- (CGFloat)cropCircleRadius
{
    NSNumber *circleRadius = objc_getAssociatedObject(self, &blockKeyCropCircleRadius);
    return [circleRadius floatValue];
}
@end




@implementation UIColor (UIColorCategory)

- (BOOL)isEqualToColor:(UIColor *)otherColor
{
    CGColorSpaceRef colorSpaceRGB = CGColorSpaceCreateDeviceRGB();
    
    UIColor *(^convertColorToRGBSpace)(UIColor*) = ^(UIColor *color) {
        if(CGColorSpaceGetModel(CGColorGetColorSpace(color.CGColor)) == kCGColorSpaceModelMonochrome) {
            const CGFloat *oldComponents = CGColorGetComponents(color.CGColor);
            CGFloat components[4] = {oldComponents[0], oldComponents[0], oldComponents[0], oldComponents[1]};
            return [UIColor colorWithCGColor:CGColorCreate(colorSpaceRGB, components)];
        } else
            return color;
    };
    
    UIColor *selfColor = convertColorToRGBSpace(self);
    otherColor = convertColorToRGBSpace(otherColor);
    CGColorSpaceRelease(colorSpaceRGB);
    
    return [selfColor isEqual:otherColor];
}
- (CGFloat)red
{
//    CGFloat red = 0.0, green = 0.0, blue = 0.0, alpha =0.0;
//    [self getRed:&red green:&green blue:&blue alpha:&alpha];

    const CGFloat* components = CGColorGetComponents(self.CGColor);
    return components[0];
}
- (CGFloat)green
{
    const CGFloat* components = CGColorGetComponents(self.CGColor);
    return components[1];
}
- (CGFloat)blue
{
    const CGFloat* components = CGColorGetComponents(self.CGColor);
    return components[2];
}
- (CGFloat)alpha
{
    return CGColorGetAlpha(self.CGColor);
}

+ (UIColor *)colorWithHexString:(NSString *)str {
    const char *cStr = [str cStringUsingEncoding:NSASCIIStringEncoding];
    long x = strtol(cStr+1, NULL, 16);
    return [UIColor colorWithHex: (UInt32)x ];
}

+ (UIColor *)colorWithHex:(UInt32)col {
    unsigned char r, g, b;
    b = col & 0xFF;
    g = (col >> 8) & 0xFF;
    r = (col >> 16) & 0xFF;
    return [UIColor colorWithRed:(float)r/255.0f green:(float)g/255.0f blue:(float)b/255.0f alpha:1];
}


@end

