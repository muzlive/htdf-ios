//
//  RatingPopUp.h
//  HTDF
//
//  Created by Haetaek Lee on 2017. 2. 12..
//  Copyright © 2017년 xSync. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HCSStarRatingView.h"

@interface RatingPopUp : UIViewController

+ (void)presentPopUpTo:(UIViewController *)viewControllerToPresent withData:(NSDictionary *)companyData completion:(void(^)())completion;
+ (void)presentPopUpTo:(UIViewController *)viewControllerToPresent
           withVOVData:(NSDictionary *)vovData
            completion:(void(^)())completion;

@property (nonatomic, copy) void(^completion)();

@property (nonatomic, strong)
    NSString *mode;

@property (nonatomic, strong) NSDictionary *ratingData;
@property (weak, nonatomic) IBOutlet UIView *viewNotice;
@property (weak, nonatomic) IBOutlet UILabel *labelContents;

@property (weak, nonatomic) IBOutlet UIView *viewPopUpBody;
@property (weak, nonatomic) IBOutlet UILabel *labelTitle;
@property (weak, nonatomic) IBOutlet HCSStarRatingView *viewStarRating;

- (IBAction)clickResigner:(id)sender;
- (IBAction)clickRating:(id)sender;

@end
