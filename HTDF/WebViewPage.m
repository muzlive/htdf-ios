//
//  NoticeDetailPage.m
//  PickInstead
//
//  Created by Haetaek Lee on 2015. 7. 28..
//  Copyright (c) 2015년 uxight. All rights reserved.
//

#import "WebViewPage.h"
#import "Functions.h"

@interface WebViewPage ()
@end

@implementation WebViewPage


- (IBAction)clickClose:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self loadPageWithUrlString:_url];
    
//    if (_isModal==NO) {
//        NSMutableArray *items = [NSMutableArray arrayWithArray:_toolbar.items];
//        [items removeObjectAtIndex:0];
//        _toolbar.items = items;
//    }
}

- (void)loadPageWithUrlString:(NSString *)urlString
{
    [_webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:urlString]]];
}

- (void)webViewDidStartLoad:(UIWebView *)webView
{
    [_loading startAnimating];
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    [_loading stopAnimating];
}



- (UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

@end
