//
//  SKKRequestManager.h
//  SK Knights
//
//  Created by 김기범 on 2016. 10. 17..
//  Copyright © 2016년 xSync. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AFHTTPSessionManager.h"



@interface RequestManager : NSObject
@property (nonatomic, weak) id delegate;

- (void)getCurrentPerformanceDataWithCompletion:(void(^)(NSDictionary *))completion;
/// 지도에 표시될 정보들 받아오기
- (void)getCompanyInfoListWithCompletion:(void(^)(NSArray *))completion;
/// 관심 업체들 목록
- (void)getFavoriteListWithCompletion:(void(^)(NSArray *))completion;
/// 관심 업체 추가
- (void)addToFavoriteListWithId:(int)companyId completion:(void(^)(BOOL))completion;
/// 관심 업체 제거
- (void)removeFromFavoriteListWithId:(int)companyId completion:(void(^)(BOOL))completion;
/// 평가하기
- (void)rateCompanyWithId:(int)companyId rating:(CGFloat)rate completion:(void(^)(BOOL))completion;

-(void)checkUser:(NSString*)udid
       pushToken:(NSString *)pushToken
      completion:(void(^)(NSDictionary *))completion;

// VOV 있는지 체크하기
- (void)checkVovWithCompletion:(void(^)(NSDictionary *))completion;

// VOV Rate하기
- (void)rateVOVWithId:(int)vovId
               rating:(CGFloat)rate
           completion:(void(^)(BOOL))completion;

- (void)setPushResponseChecked:(NSString *)pushId
                    completion:(void(^)(BOOL))completion;


@end
