//
//  PopupViewController.m
//  Cultwo
//
//  Created by ParkJongsung on 2016. 12. 11..
//  Copyright © 2016년 xSync. All rights reserved.
//

#import "PopupViewController.h"
#import "Functions.h"

@interface PopupViewController ()
@property (weak, nonatomic) IBOutlet UIView *viewPopupWrapper;
@property (weak, nonatomic) IBOutlet UIView *viewPopupIng;
@property (weak, nonatomic) IBOutlet UIView *viewPopupText;
@property (weak, nonatomic) IBOutlet UIButton *buttonComplete;

- (IBAction)clickComplete:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *labelPopupText;

@end

@implementation PopupViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(connectionDidFailed) name:@"connectError" object:nil];

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(setPopupWithMessage:) name:@"popupWithMessage" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(setPopupWithLoading) name:@"popupLoading" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(closePopup) name:@"closePopup" object:nil];
    
    
    [self.buttonComplete.layer setBorderWidth:2.0f];
    [self.buttonComplete.layer setBorderColor:[[UIColor colorWithHexString:@"#5B4526"] CGColor]];
    [self.viewPopupWrapper.layer setBorderWidth:3.0f];
    [self.viewPopupWrapper.layer setBorderColor:[[UIColor colorWithHexString:@"#5B4526"] CGColor]];

    // Do any additional setup after loading the view.
}

-(void)closePopup{
    self.view.superview.alpha = 0;
    self.view.superview.userInteractionEnabled = NO;
}
-(void)setPopupWithLoading{
    self.view.superview.alpha = 1;
    self.view.superview.userInteractionEnabled = YES;
    [self _setPopupLoading];
}


-(void)setPopupWithMessage:(NSNotification *)notif{
    NSDictionary *userInfo = notif.userInfo;
    self.view.superview.alpha = 1;
    self.view.superview.userInteractionEnabled = YES;
    [self _setPopupText:[userInfo objectForKey:@"message"]];
}




-(void)connectionDidFailed{
    self.view.superview.alpha = 1;
    self.view.superview.userInteractionEnabled = YES;
    
    [self _setPopupText:@"네트워트 연결이 불안정합니다.\n와이파이 혹은 데이터 네트워크를\n확인해주세요."];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)clickComplete:(id)sender {
    self.view.superview.alpha = 0;
    self.view.superview.userInteractionEnabled = NO;
}


-(void)_setPopupText:(NSString *)text{
    self.labelPopupText.text = text;
    self.viewPopupText.hidden = NO;
    self.viewPopupIng.hidden = YES;
}
-(void)_setPopupLoading{
    self.viewPopupText.hidden = YES;
    self.viewPopupIng.hidden = NO;
}

@end
