//
//  NoticeDetailPage.h
//  PickInstead
//
//  Created by Haetaek Lee on 2015. 7. 28..
//  Copyright (c) 2015년 uxight. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WebViewPage : UIViewController <UIWebViewDelegate>

@property (nonatomic, assign) BOOL isModal;
@property (nonatomic, strong) NSString *url;

- (IBAction)clickClose:(id)sender;
@property (weak, nonatomic) IBOutlet UIWebView *webView;
@property (weak, nonatomic) IBOutlet UIToolbar *toolbar;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *loading;

@end
