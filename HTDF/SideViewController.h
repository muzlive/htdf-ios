//
//  SideViewController.h
//  Cultwo
//
//  Created by ParkJongsung on 2016. 12. 11..
//  Copyright © 2016년 xSync. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WebViewController.h"



@interface SideViewController : UIViewController

@property (nonatomic, weak) WebViewController *superWebVC;

@property (weak, nonatomic) IBOutlet UIView *viewTicketContainer;

- (IBAction)clickMenuButton:(id)sender;
- (IBAction)clickEntrance:(id)sender;

- (IBAction)clickNotice:(id)sender;
- (IBAction)clickConference:(id)sender;
- (IBAction)clickMap:(id)sender;
- (IBAction)clickSpecialFeature:(id)sender;
- (IBAction)clickSns:(id)sender;
- (IBAction)clickFeedback:(id)sender;
- (IBAction)clickSupporter:(id)sender;
- (IBAction)clickSecurity:(id)sender;
- (IBAction)clickContactUs:(id)sender;
- (IBAction)clickBooth:(id)sender;

@end
