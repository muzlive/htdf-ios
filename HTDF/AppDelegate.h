//
//  AppDelegate.h
//  SK Knights
//
//  Created by 김기범 on 2016. 10. 13..
//  Copyright © 2016년 xSync. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

