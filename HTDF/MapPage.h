//
//  MapPage.h
//  HTDF
//
//  Created by Haetaek Lee on 2017. 1. 30..
//  Copyright © 2017년 xSync. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SelectedBoothCell.h"


@interface MapPage : UIViewController <UIScrollViewDelegate, UITableViewDelegate, UITableViewDataSource, SelectedBoothCellProtocol>

@property (nonatomic, strong) NSArray *boothInfoList;
@property (nonatomic, strong) NSMutableArray *favoriteBoothList;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintSelectedViewBottom;
@property (weak, nonatomic) IBOutlet UIView *viewSelectedBooth;
@property (weak, nonatomic) IBOutlet UITableView *tableViewSelectedBooth;

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIView *viewContainer;

@end
