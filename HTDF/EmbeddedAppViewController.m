
#import "EmbeddedAppViewController.h"


@interface EmbeddedAppViewController ()
@property (strong, nonatomic) IBOutlet UIWebView *webView;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@end

@implementation EmbeddedAppViewController

#define GET_VALUE(key) [[NSUserDefaults standardUserDefaults]objectForKey:key]
#define SET_VALUE(value, key) {[[NSUserDefaults standardUserDefaults]setObject:value forKey:key]; [[NSUserDefaults standardUserDefaults] synchronize];}


- (void)viewDidLoad {
    [super viewDidLoad];
    _webView.delegate = self;
    _webView.scrollView.bounces = NO;
    _activityIndicator.layer.opacity = 0;
    // [self setJavascriptBridge];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}



-(BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType{
    NSString *requestString = [[[request URL] absoluteString] stringByReplacingPercentEscapesUsingEncoding: NSUTF8StringEncoding];
    
  // WebView Log볼시 사용. Debug용도.
    if ([requestString hasPrefix:@"ios-log:"]) {
        NSString* logString = [[requestString componentsSeparatedByString:@":#iOS#"] objectAtIndex:1];
        NSLog(@"UIWebView console: %@", logString);
        return NO;
    }
    return YES;
}



- (void)loadWebPageWithURLString:(NSString *)URLString {
    URLString = [URLString componentsSeparatedByString:@" "][0];
    NSURL *url = [NSURL URLWithString:URLString];
    NSURLRequest *request = [NSURLRequest requestWithURL:url
                                             cachePolicy:NSURLRequestReloadIgnoringCacheData
                                         timeoutInterval:30];
    [self.webView loadRequest:request];
}

- (void)webViewDidStartLoad:(UIWebView *)webView {
    [UIView animateWithDuration:0.2 animations:^{
        self.activityIndicator.layer.opacity = 1;
    }];
}

- (void)webViewDidFinishLoad:(UIWebView *)webView {
    [UIView animateWithDuration:0.2 animations:^{
        self.activityIndicator.layer.opacity = 0;
    }];
}

@end
