//
//  SecurityPage.h
//  HTDF
//
//  Created by Haetaek Lee on 2017. 2. 13..
//  Copyright © 2017년 xSync. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SecurityPage : UIViewController<UIScrollViewDelegate>

@end
