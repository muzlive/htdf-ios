

#import <UIKit/UIKit.h>
#import "WebViewController.h"

@interface EmbeddedAppViewController : UIViewController <UIWebViewDelegate>

- (void)loadWebPageWithURLString:(NSString *)URLString;

@end
