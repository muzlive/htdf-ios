//
//  WebViewController.h
//  SK Knights
//
//  Created by 김기범 on 2016. 10. 19..
//  Copyright © 2016년 xSync. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "PushManager.h"

@interface WebViewController : UIViewController <UIWebViewDelegate>
@property NSString *urlString;

// 웹뷰에서 요청한 소켓통신 메소드 저장하는 dictionary
@property (strong, nonatomic) NSMutableDictionary *requestDic;

@property (weak, nonatomic) IBOutlet UILabel *labelBarTitle;
@property (weak, nonatomic) IBOutlet UIView *containerView;
@property (weak, nonatomic) IBOutlet UIView *containerViewForSecurity;

@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;


@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topbarHeightConstraint;

- (void)loadWebsite:(NSString *)URLString ;

@end
