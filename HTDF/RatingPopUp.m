//
//  RatingPopUp.m
//  HTDF
//
//  Created by Haetaek Lee on 2017. 2. 12..
//  Copyright © 2017년 xSync. All rights reserved.
//

#import "RatingPopUp.h"
#import "RequestManager.h"
#import "Functions.h"

#define COMPANY_RATING @"COMPANY_RATING"
#define VOV_RATING @"VOV_RATING"
#define VOV_NOTICE @"VOV_NOTICE"

@interface RatingPopUp ()
@end

@implementation RatingPopUp


+ (void)presentPopUpTo:(UIViewController *)viewControllerToPresent
              withData:(NSDictionary *)companyData
            completion:(void(^)())completion {
    RatingPopUp *popUp = [[RatingPopUp alloc]init];
    popUp.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    popUp.mode = COMPANY_RATING;
    popUp.ratingData = companyData;
    popUp.completion = completion;

    
    [viewControllerToPresent presentViewController:popUp animated:NO completion:nil];
}

+ (void)presentPopUpTo:(UIViewController *)viewControllerToPresent
              withVOVData:(NSDictionary *)vovData
            completion:(void(^)())completion {
    NSArray *vov_list = GET_VALUE(@"vov_list");
    if( !vov_list ){
        vov_list = [NSMutableArray array];
    }
    if( [vov_list containsObject:
         [NSString stringWithFormat:@"%@,%@",
          vovData[@"type"], vovData[@"id"]]]){
        return;
    }
    NSMutableArray *mutableVovList =
            [NSMutableArray arrayWithArray:vov_list];
    
    [mutableVovList addObject:[NSString stringWithFormat:@"%@,%@",
                               vovData[@"type"], vovData[@"id"]]];
    SET_VALUE(mutableVovList, @"vov_list");
    
    RatingPopUp *popUp = [[RatingPopUp alloc]init];
    popUp.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    
    if( [vovData[@"type"] isEqualToString:@"vov_vote"]){
        popUp.mode = VOV_RATING;
    }else {
        popUp.mode = VOV_NOTICE;
    }
    
    popUp.ratingData = vovData;
    popUp.completion = completion;
    
    [viewControllerToPresent presentViewController:popUp
                                          animated:NO completion:nil];
    
}


- (void)viewDidLoad {
    [super viewDidLoad];
    
    if( [self.mode isEqualToString:COMPANY_RATING]){
        self.labelTitle.text = [NSString
                                stringWithFormat:@"%@에 대해 평점을 남겨주세요", self.ratingData[@"name"]];
        
        [self.viewStarRating setHidden:NO];
        [self.viewNotice setHidden:YES];
        
    }else if( [self.mode isEqualToString:VOV_NOTICE]){
        self.labelTitle.text = self.ratingData[@"message"];
        self.labelContents.text = self.ratingData[@"contents"];
        
        [self.viewNotice setHidden:NO];
        [self.viewStarRating setHidden:YES];
        
    }else {
        [self.viewNotice setHidden:YES];
        [self.viewStarRating setHidden:NO];
        self.labelTitle.text = self.ratingData[@"message"];
    }
    _viewPopUpBody.layer.cornerRadius = 3.0;
}


- (IBAction)clickResigner:(id)sender {
    [self dismissViewControllerAnimated:NO completion:nil];
}

- (IBAction)clickRating:(id)sender {
    int ratingId = [self.ratingData[@"id"] intValue];
    __weak typeof(self) weakSelf = self;
    
    if( [self.mode isEqualToString:COMPANY_RATING]){
        [[[RequestManager alloc] init]
         rateCompanyWithId:ratingId
         rating:self.viewStarRating.value completion:^(BOOL success) {
             if (weakSelf.completion) {
                 _completion();
             }
             [weakSelf dismissViewControllerAnimated:NO completion:nil];
         }];

    }else {
        [[[RequestManager alloc] init]
         rateVOVWithId:ratingId
         rating:self.viewStarRating.value
         completion:^(BOOL success) {
             if (weakSelf.completion) {
                 _completion();
             }
             [weakSelf dismissViewControllerAnimated:NO completion:nil];
         }];
    }
}




- (IBAction)clickCloseButton:(id)sender {
    [self dismissViewControllerAnimated:NO completion:nil];
}
@end
