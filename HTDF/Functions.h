//
//  Functions.h
//
//  Copyright UXight
//

//  2014.10.13  RENEWAL based on ARC
//  2014.10.21  Y_BOTTOM_OF_STATUSBAR 메크로 추가


//  update  ->  UIAlert 관련 업데이트

//              +saveData:toDocumentsDirectory:, +getFilePathFromDocumentsDirectory:directoryPath 추가
//              +removeDataAtPath: 추가
//              editing 모드에서 crop영역 설정 가능하게 -setCropRectSize:, -setCropCircleRadius:, -cropRectSize, -cropCircleRadius 추가
// 2014.10.8    saveData:toDocumentsDirectory: 동작 변경에 따른 사용방식 변경, #1 참고
// 2014.11.15   setTapDelegate: 싱글탭과 더블탭을 동시에 쓰는 경우 [singleTap requireGestureRecognizerToFail:doubleTap]; 처리를 하기위해 제스처 리턴
// 2014.12.23   -getTableViewCell 에서 self가 [UITableViewCell class]일 경우 추가
// 2015.01.22   showAlertWithTitle:에 if (superViewController.presentedViewController) {superViewController = superViewController.presentedViewController;} 추가
// 2015.3.14    NSDate 카테고리 +dateWithDay:month:year: 추가
// 2015.10.19   -startLoadingAnimationOnCenter 에서 tag만으로 불러오던 걸 태그가 겹치는 뷰가 있을 수 있으므로 클래스까지 검사하는 로직으로 변경
//              -getActivityIndicatorView 추가
// 2015.11.22   +isValidEmail: 추가
// 2016.01.14   +showAlertWithTitle:message: 수정. superViewController = superViewController.presentedViewController; 한번만 하지 않고 재귀적으로 되도록



#define Y_BOTTOM_OF_STATUS_BAR           (SYSTEM_VERSION_GREATER_THAN(@"7.0")? \
([UIApplication sharedApplication].statusBarHidden? 0 : CGRectGetMaxY([UIApplication sharedApplication].statusBarFrame)):0)

#define SCREEN_WIDTH            [UIScreen mainScreen].bounds.size.width
#define SCREEN_HEIGHT           [UIScreen mainScreen].bounds.size.height


#define STRING_INTEGER(integer)         [NSString stringWithFormat:@"%ld",(long)integer]
#define STRING_INT(int)                 [NSString stringWithFormat:@"%d",int]
#define STRING_FLOAT(float)             [NSString stringWithFormat:@"%f",float]
#define STRING_OBJECT(object)           [NSString stringWithFormat:@"%@", object]
#define STRING_EMPTY_IF_NULL(string)    (string==nil? @"" : string)



#define GET_VALUE(key) [[NSUserDefaults standardUserDefaults]objectForKey:key]
#define SET_VALUE(value, key) {[[NSUserDefaults standardUserDefaults]setObject:value forKey:key]; [[NSUserDefaults standardUserDefaults] synchronize];}


#define GET_BOOL_VALUE(key) [[NSUserDefaults standardUserDefaults] boolForKey:key]
#define SET_BOOL_VALUE(value, key) { [[NSUserDefaults standardUserDefaults] setBool:value forKey:key]; [[NSUserDefaults standardUserDefaults] synchronize];}



#define GET_PLIST(key)                \
[[NSUserDefaults standardUserDefaults]objectForKey:key]
#define SET_PLIST(value, key)         \
{[[NSUserDefaults standardUserDefaults]setObject:value forKey:key]; [[NSUserDefaults standardUserDefaults] synchronize];}
#define REMOVE_PLIST(key)             \
{[[NSUserDefaults standardUserDefaults]removeObjectForKey:key]; [[NSUserDefaults standardUserDefaults] synchronize];}


#define RGB(r,g,b)              [UIColor colorWithRed:r/255.0 green:g/255.0 blue:b/255.0 alpha:1.0]
#define RGBA(r,g,b,a)           [UIColor colorWithRed:r/255.0 green:g/255.0 blue:b/255.0 alpha:a]
#define HEX(rgbValue) \
    [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 \
                    green:((float)((rgbValue & 0x00FF00) >>  8))/255.0 \
                     blue:((float)((rgbValue & 0x0000FF) >>  0))/255.0 \
                    alpha:1.0]


#define IMAGE_PNG(name)         [UIImage imageWithContentsOfFile: [[NSBundle mainBundle]pathForResource:name ofType:@"png"] ]
#define IMAGE(name, extionsion) [UIImage imageWithContentsOfFile: [[NSBundle mainBundle]pathForResource:name ofType:extionsion] ]
#define IMAGE_RETINA(name)      [UIImage imageWithContentsOfFile: [[NSBundle mainBundle]pathForResource:[name stringByAppendingString:@"@2x"] ofType:@"png"] ]


#define DEGREE_TO_RADIAN(degree)   degree*(M_PI/180)
#define RADIAN_TO_DEGREE(radian)   radian*(180/M_PI)


#define FONT(fontSize)          [UIFont systemFontOfSize:fontSize]
#define BOLDFONT(fontSize)      [UIFont boldSystemFontOfSize:fontSize]


#define LOCAL_PATH(path)        \
[[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0] stringByAppendingPathComponent:path]


#define SYSTEM_VERSION_EQUAL_TO(v)                  \
([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedSame)
#define SYSTEM_VERSION_GREATER_THAN(v)              \
([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedDescending)
#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  \
([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN(v)                 \
([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN_OR_EQUAL_TO(v)     \
([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedDescending)



#import <Foundation/Foundation.h>
#import <objc/runtime.h>
//#import <AVFoundation/AVFoundation.h>
#import <MessageUI/MessageUI.h>
#import <MessageUI/MFMailComposeViewController.h>
#import <QuartzCore/QuartzCore.h>
#import <sys/utsname.h>

// + presentActionSheetInViewController:title:message:cancelButtonTitle:destructiveButtonTitle:otherButtonTitles:delegate; 에서 사용
static NSInteger const UIActionSheetCancelButtonIndex = 0;
static NSInteger const UIActionSheetDestructiveButtonIndex = 1;
static NSInteger const UIActionSheetFirstOtherButtonIndex = 2;

@interface Functions : NSObject <MFMailComposeViewControllerDelegate>



/**
 *  needs :  #import <sys/utsname.h>
 *  device의 종류(아이폰5, 6 등)를 알고 싶을 때
 *  @return 
                @"i386"      on the simulator
                @"iPod1,1"   on iPod Touch
                @"iPod2,1"   on iPod Touch Second Generation
                @"iPod3,1"   on iPod Touch Third Generation
                @"iPod4,1"   on iPod Touch Fourth Generation
                @"iPhone1,1" on iPhone
                @"iPhone1,2" on iPhone 3G
                @"iPhone2,1" on iPhone 3GS
                @"iPad1,1"   on iPad
                @"iPad2,1"   on iPad 2
                @"iPad3,1"   on iPad 3 (aka new iPad)
                @"iPhone3,1" on iPhone 4
                @"iPhone4,1" on iPhone 4S
                @"iPhone5,1" on iPhone 5
                @"iPhone5,2" on iPhone 5
 */
NSString* deviceName();

CGFloat yBottomOfStatusBar();


/// 메인스레드에서 수행
void runOnMainQueueWithoutDeadlocking(void (^block)(void));

// ----------------------------------------------------------------------------------------------------
// Singleton
//
+ (Functions *)sharedInstance;


// ----------------------------------------------------------------------------------------------------
// use plist for save&load
//
// general
//
/**
 * plist에 key-value로 NSString이 아닌 value값을 저장.
 * 스트링일 경우는 SET_PLIST(value, key) 사용
 * @return
 * @param   id value, NSString key
 */
+ (void)saveDataToPlist:(id)data forKey:(NSString *)key;
+ (id)loadDataFromPlistForKey:(NSString *)key;
/// plist에 key-value로 저장된 모든 값을 초기화
+ (void)removeAllDataInPlist;
//
// specify ARRAY
//
/// 내부 동작은 -saveDataToPlist:forKey: 와 완전히 동일. 캐스팅을 해줄 필요없이 사용할 수 있는 정도
+ (void)saveArrayToPlist:(NSMutableArray *)array forKey:(NSString *)key;
/**
 * @return  없을 경우 nil 이 아니라 빈 array 리턴
 * @param   NSString 키
 */
+ (NSMutableArray *)loadArrayFromPlistForKey:(NSString *)key;
/// 해당 key로 plist에 저장된 배열 비우기
+ (void)removeAllObjectsInArrayFromPlistForKey:(NSString *)key;




// 모달띄울때 TopMost Controller 가져오기.
+(UIViewController *)topMostController;



// ----------------------------------------------------------------------------------------------------
// needs :  #import <AVFoundation/AVFoundation.h>
//
//+ (AVAudioPlayer *)audioPlayerWithRetainObject:(id)retainObject;
//+ (void)audioPlayerWithRetainObject:(id)retainObject playURL:(NSURL *)url volume:(CGFloat)volume numberOfLoops:(NSInteger)numberOfLoops;


// ----------------------------------------------------------------------------------------------------
// needs :  #import <AVFoundation/AVFoundation.h>
//          CoreMedia.framework
//
//+ (BOOL)trimAudioWithInputURL:(NSURL *)audioFileInput outPutURL:(NSURL *)audioFileOutput startFrom:(float)vocalStartMarker endTo:(float)vocalEndMarker;



// ----------------------------------------------------------------------------------------------------
// needs :  #import <MessageUI/MessageUI.h>
//          #import <MessageUI/MFMailComposeViewController.h>
//

/// Send Mail
+ (MFMailComposeViewController *)presentMailComposeViewControllerTo:(UIViewController *)superViewController
                                                       toRecipients:(NSArray *)toRecipients
                                                            subject:(NSString *)subject
                                                        messageBody:(NSString *)messageBody;

+ (MFMailComposeViewController *)presentMailComposeViewControllerTo:(UIViewController *)superViewController
                                                           animated:(BOOL)animated
                                                         completion:(void(^)())completion
                                                       toRecipients:(NSArray *)toRecipients
                                                            subject:(NSString *)subject
                                                        messageBody:(NSString *)messageBody
                                                           delegate:(void(^)(MFMailComposeViewController *controller, MFMailComposeResult result, NSError *error))delegate;


/// Send SMS
+ (MFMessageComposeViewController *)presentMessageComposeViewControllerTo:(UIViewController *)superViewController
                                                               recipients:(NSArray *)recipient
                                                                     body:(NSString *)body
                                                                 delegate:(void(^)(MessageComposeResult result))delegate;

+ (MFMessageComposeViewController *)presentMessageComposeViewControllerTo:(UIViewController *)superViewController
                                                                 animated:(BOOL)animated
                                                               completion:(void(^)())completion
                                                               recipients:(NSArray *)recipient
                                                                     body:(NSString *)body
                                                                 delegate:(void(^)(MessageComposeResult result))delegate;


// ----------------------------------------------------------------------------------------------------
// 시간과 문자열에 관한 함수들
//
/// 현재 시간(timestamp) 문자열 생성
+ (NSString *)timestampString;
/// 초를 --분:--초 형식으로 변환 (예: 370 -> @"06:10")
+ (NSString *)convertTommssFormatStringFromSeconds:(NSInteger)totalSec;
/// 초를 --시간 --분 --초 형식으로 변환 (예: 1870 -> @"05시간 01분 10초")
+ (NSString *)convertToHHmmssFormatStringFromSeconds:(NSInteger)totalSec;
/// 초를 -일 -시간 -분 -초 형식으로 변환 (예: 88270 -> @"1일 5시간 1분 10초") *앞에 0 없음
+ (NSString *)convertToddHHmmssFormatStringFromSeconds:(NSInteger)totalSec;


// ----------------------------------------------------------------------------------------------------
//

/// 두 점 사이의 거리 구하기
CGFloat distanceBetweenPoint(CGPoint pt1, CGPoint pt2);
+ (CGFloat)distanceBetweenPoint1:(CGPoint)pt1 point2:(CGPoint)pt2;


/// 리소스 파일로 NSString 문자열 불러오기. (fileName : 리소스 폴더의 파일 이름, extension : 확장자)
+ (NSString *)loadStringWithPathForResource:(NSString *)fileName ofType:(NSString *)extension;

// sortUsingComparator:로 구현 가능
//+ (void)sortArray:(NSMutableArray *)unsortedDatesArray;


// UIImagePickerController
+ (UIImagePickerController *)presentImagePickerController:(UIViewController *)superViewController
                                                 animated:(BOOL)animated
                                        presentCompletion:(void(^)())presentCompletion
                                            allowsEditing:(BOOL)allowsEditing
                                               sourceType:(UIImagePickerControllerSourceType)sourceType
                                         didFinishPicking:(void(^)(UIImagePickerController *picker, NSDictionary *info, UIImage *image))didFinishPicking
                                                didCancel:(void(^)(UIImagePickerController *picker))didCancel;

/// get app version
+ (NSString *)bundleVersion;

/// get localization
+ (NSString *)appleLanguage;


// get Date String like 3분전
+(NSString *)getDateString:(NSString *)date;


// needs :  #import <QuartzCore/QuartzCore.h>
//
// (duration)동안 (rotations)바퀴 만큼 회전, (repeat)번 반복
+ (void)runSpinAnimationOnView:(UIView*)view duration:(CGFloat)duration rotations:(CGFloat)rotations repeat:(float)repeat;



/**
 * 해당 앱의 데이터를 저장하는 공간에 데이터를 저장
 * @return  BOOL 성공 여부
 * @param   directoryPath는 앱 내부 저장 경로 뒤에 붙는 sub path. eg) (내부 저장 경로/)forder/fileName.png
 */
+ (BOOL)saveData:(NSData *)data toDocumentsDirectory:(NSString *)directoryPath;
/**
 * 내가 지정한 directoryPath 는 파일의 full path 가 아니므로 -imageWithContentsOfFile: 등으로 파일을 불러올 때는 해당 함수로 불러온 full path를 사용
 * !! full path는 앱 실행시 마다 변하므로 저장해놓고 사용하면 안되고 directoryPath만 저장해놨다가 해당 함수로 full path로 변환 후 사용해야 한다
 * @return  앱 내부 저장 경로/directoryPath 의 Full Path
 * @param   directoryPath 는 내가 지정한 앱 내부 저장 경로속의 path
 */
+ (NSString *)getFilePathFromDocumentsDirectory:(NSString *)directoryPath;
/// 해당 경로에 저장된 데이터를 삭제
+ (void)removeDataAtDocumentsDirectoryPath:(NSString *)directoryPath;

/// validate email format
+ (BOOL)isValidEmail:(NSString*)emailString;

// check version (appstoreID ex:@"378458261")
+ (void)receiveAppInfoWithID:(NSString *)appStoreID callBack:(void(^)(NSDictionary *appData ,NSString *version))callBack;
+ (void)presentAppStoreForID:(NSNumber *)appStoreID toSuperViewController:(UIViewController *)superViewController didFinish:(void(^)(UIViewController *viewController))didFinishBlock;



+ (NSString *)getDeviceUUID;

/************************************************************************************************************************
 *  UIAlertView 가 deprecated 되면서 iOS8 이상에서는 UIAlertController를 사용, 이하 버전에서는 UIAlertView를 사용하도록 분기 처리
 */
+ (void)showAlertWithTitle:(NSString *)title message:(NSString *)message buttonTitles:(NSArray *)buttons delegate:(void(^)(NSInteger buttonIndex))delegate;
+ (void)showAlertWithTitle:(NSString *)title message:(NSString *)message;
+ (void)showAlertNetwordErrorMessage;


+ (id)presentActionSheetInViewController:(UIViewController *)viewController
                               withTitle:(NSString *)title
                                 message:(NSString *)message
                       cancelButtonTitle:(NSString *)cancelButtonTitle
                  destructiveButtonTitle:(NSString *)destructiveButtonTitle
                       otherButtonTitles:(NSArray *)otherButtonTitles
                                delegate:(void(^)(UIAlertController *alertController, UIAlertAction *action, NSInteger buttonIndex))delegate;
+ (id)presentActionSheetWithTitle:(NSString *)title
                          message:(NSString *)message
                cancelButtonTitle:(NSString *)cancelButtonTitle
           destructiveButtonTitle:(NSString *)destructiveButtonTitle
                otherButtonTitles:(NSArray *)otherButtonTitles
                         delegate:(void(^)(UIAlertController *alertController, UIAlertAction *action, NSInteger buttonIndex))delegate;

@end





// -------------------------------------------------- Category .h -------------------------------------------------- //
//

@interface NSObject (PerformBlockAfterDelay)

/// 해당 블럭에 있는 코드를 (delay)초 이후에 실행, !! 이 메서드를 사용한 클래스가 해제되어야 하는 시간 이후까지 delay가 걸려 있으면 클래스가 해제되지 않고 해당 블럭을 계속 실행
- (void)performBlock:(void (^)(void))block afterDelay:(NSTimeInterval)delay;

- (id)nilIfNSNull;

@end




@interface UIAlertView (UIAlertViewCategory)

#define ALERT_DEFAULT_BUTTON_SET        @[NSLocalizedString(@"OK",@"locailzed"), NSLocalizedString(@"Cancel",@"locailzed")]
#define ALERT_DEFAULT_BUTTON_OK         NSLocalizedString(@"OK", @"localized")
#define ALERT_DEFAULT_BUTTON_CANCEL     NSLocalizedString(@"Cancel", @"localized")
#define ALERT_TITLE_NETWORK_ERROR       NSLocalizedString(@"Network error", @"localized")
#define ALERT_MESSAGE_NETWORK_ERROR     NSLocalizedString(@"An error occurred. \nPlease try again later", @"localized")

/// 눌렀을 때의 딜리게이트를 블럭으로 처리. buttons에는 버튼에 들어갈 문자열을 배열로 넘겨줌, 버튼배열의 인덱스가 버튼의 인덱스이며 배열이 nil일 경우 딜리게이트없이 [확인] 버튼만 노출
+ (void)showWithTitle:(NSString *)title message:(NSString *)message buttonTitles:(NSArray *)buttons delegate:(void(^)(NSInteger buttonIndex))delegate;
+ (void)showWithTitle:(NSString *)messageTitle message:(NSString *)message;
+ (void)showNetwordErrorMessage;

// UIActivityIndicatorView가 돌아가는 AlertView 띄우기
+ (id)showAlertViewWithLoadingAnimationWithTitle:(NSString *)title;
+ (void)dismissAlertViewWithLoadingAnimation;

/// AlertView가 존재하는지 여부 - deprecated
+ (BOOL)doesAlertViewExist;
/// 해당 title을 갖는 AlertView가 존재하는지 여부 - deprecated
+ (BOOL)doesAlertViewExistWithTitle:(NSString *)title;

@end





@interface NSString (NSStringCategory)

// NSString 문자열을 해당 포맷(@"yyyy-MM-dd HH:mm:ss" 등)의 NSDate 날짜로 변환
- (NSDate *)dateWithDateFormat:(NSString *)dateFormat;

/// @"1000", @"-20000" 등의 문자열을 돈을 표시할 때 처럼 @"1,000", @"-20,000" 등의 형식으로 표시, 단위는 localize 문제로 포함하지 않음
- (NSString *)moneyFormat;
/// @"1,000", @"-20,000" 등의 돈 형식 문자열을 @"1000", @"-20000" 등의 숫자 문자열 복원
- (NSString *)restoreMoneyFormat;
/// @"1,000", @"-20,000" 등의 돈 형식 문자열을 1000", -20000 등의 숫자로 복원
- (int)restoreMoneyFormatToInt;

- (NSString *)URLEncodedString_ch;

- (NSNumber *)NSNumberValue;

@end





@interface NSDate (NSDateCategory)

- (NSString *)monthName;
- (NSString *)weekdayName;
- (NSInteger)year;
- (NSInteger)month;
- (NSInteger)day;
/// dateFormat:nil -> (@"yyyy-MM-dd HH:mm:ss") timestamp default
- (NSString *)stringWithDateFormat:(NSString *)dateFormat;
+ (NSDate *)localDateTime;
+ (NSDate *)dateWithDay:(NSInteger)day month:(NSInteger)month year:(NSInteger)year;

@end





@interface UIView (UIViewCategory) 

- (UITableViewCell *)getTableViewCell;
- (UICollectionViewCell *)getCollectionViewCell;

/// view의 viewController를 리턴
- (UIViewController *)viewController;

/// view에 UIActivityIndicatorView를 붙이고 애니메이션, UIActivityIndicatorView의 center를 인자로 넘겨줌
- (UIActivityIndicatorView *)startLoadingAnimationOnCenter:(CGPoint)center;
- (void)stopLoadingAnimation;
/// -startLoadingAnimationOnCenter 를 통해 만들어논 ActivityIndicatorView가 있을 경우 가져옴
- (UIActivityIndicatorView *)getActivityIndicatorView;

/// 해당 view를 버튼처럼 탭제스처를 받게 만들고 눌렸을 때의 딜리게이트를 블럭으로 넘겨줌
- (UITapGestureRecognizer *)setTapDelegate:(void(^)(UITapGestureRecognizer *tapGestureRecognizer))delegateBlock;
/**
 * longpress 제스처 추가, important!! gesture.state 가 시작일 때와 끝날 때 두번 호출되므로 딜리게이트에서 분기할 것.
 * @param   minimumPressDuration가 안넘어 올 경우 기본 값
 */
- (void)setLongPressDelegate:(void(^)(UILongPressGestureRecognizer *gestureRecognizer))delegateBlock
        minimumPressDuration:(CFTimeInterval)minimumPressDuration;
/// 더블 탭
- (UITapGestureRecognizer *)setDoubleTapDelegate:(void(^)(UITapGestureRecognizer *tapGestureRecognizer))delegateBlock;

// 해당 뷰의 origin 기준으로 center
- (CGPoint)centerFromOrigin;

// 뷰의 프레임중 x,y,w,h 를 수정하고 싶을 때
- (void)setFrameX:(CGFloat)x;
- (void)setFrameY:(CGFloat)y;
- (void)setFrameWidth:(CGFloat)width;
- (void)setFrameHeight:(CGFloat)height;

- (void)setFrameOrigin:(CGPoint)origin;
- (void)setFrameSize:(CGSize)size;

- (void)removeSubviews;

- (void)setDraggable;

@end





@interface UIImage (UIImageCategory)
// resize image
- (UIImage*)resizeWithSize:(CGSize)size;
- (UIImage *)resizeWithScale:(CGFloat)scale;
- (UIImage *)resizeFitToWidth:(CGFloat)width;
- (UIImage *)resizeFitToHeight:(CGFloat)height;
//
// needs :  #import <QuartzCore/QuartzCore.h>
//          link <CoreImage.framework>
- (UIImage *)gaussianBlurWithRadius:(CGFloat)inputRadius;
@end



@interface UITextView (UITextViewCategory)
- (void)setPaddingZero;
@end



@interface UILabel (UILabelCategory)
- (UILabel *)copyWithFrame:(CGRect)rect;
- (void)setNumberWithCountingAnimation:(NSString *)numberString         // 숫자의 문자열
                        changeInterval:(NSTimeInterval)changeInterval   // 숫자가 한번 바뀔 때마다 걸리는 시간
                        numberOfChange:(NSInteger)numberOfChange;       // 총 바뀌는 횟수
- (void)setNumberWithCountingAnimation:(NSString *)numberString;
- (void)cancelCountingAnimation;
- (void)setText:(NSString *)text withSwitchAnimationColor:(UIColor *)textColor;
@end




@interface UIViewController (UIViewControllerCategory)
@end




/*
@interface UINavigationController (UINavigationControllerCategory)
// 회전 시 네비게이션 컨트롤러를 썼어도 딜리게이트가 먹히게 하도록
- (BOOL)shouldAutorotate;
- (NSUInteger)supportedInterfaceOrientations;
- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation;
@end

@interface UITabBarController (UITabBarControllerCategory)
// 회전 시 탭바 컨트롤러를 썼어도 딜리게이트가 먹히게 하도록
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation;
- (BOOL)shouldAutorotate;
- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation;
@end
*/


// UIImagePickerController bug iOS6.1 이하??
@interface UIImagePickerController (NonRotating)
- (BOOL)shouldAutorotate;
- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation;

//  temp - > 뷰만 보여지는 것이고 실제 크롭된 이미지는 기존 크롭사각형에 맞는 정사각형이미지가 리턴, setCropCircleRadius만 사용가능
- (void)setCropCircleRadius:(CGFloat)circleRadius; // 뷰가로사이즈/2 를 넘기기
- (CGFloat)cropCircleRadius;
@end



@interface UIColor (UIColorCategory)
- (BOOL)isEqualToColor:(UIColor *)otherColor;
- (CGFloat)red;
- (CGFloat)green;
- (CGFloat)blue;
- (CGFloat)alpha;
// hex color
/**
 *  @param  eg : \@"#123456"
 */
+ (UIColor *)colorWithHexString:(NSString *)str;
/**
 *  @param  eg : 0x123456
 */
+ (UIColor *)colorWithHex:(UInt32)col;
@end

