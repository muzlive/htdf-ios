//
//  WebViewController.m
//  SK Knights
//
//  Created by 김기범 on 2016. 10. 19..
//  Copyright © 2016년 xSync. All rights reserved.
//

#import "WebViewController.h"
#import "EmbeddedAppViewController.h"
#import "Functions.h"
#import <ViewDeck/ViewDeck.h>
#import "Common.h"
#import "PopupViewController.h"
#import "SideViewController.h"
#import "RequestManager.h"

@interface WebViewController ()

@property BOOL didEnterRecordPage;

@property (strong, nonatomic) IBOutlet UIWebView *webView;
@property (strong, nonatomic) RequestManager *requestManager;

@property (weak, nonatomic) IBOutlet UIView *popupContainer;


@property BOOL didJukeboxOrSelfCameraAppear;
- (IBAction)clickMenuButton:(id)sender;


@end
#define kHTDFHome @"http://211.249.62.55:8060"
#define kHTDFIntro @"https://xsync.typeform.com/to/mgl7KO"

#define kHeightOfMenuBar 74



@implementation WebViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // web view init
    self.webView.scrollView.bounces = NO;
    self.webView.scalesPageToFit = YES;
    self.webView.scrollView.scrollEnabled = YES;
    self.webView.scrollView.maximumZoomScale = 1.0f;
    self.webView.scrollView.minimumZoomScale = 1.0f;
    
    self.webView.delegate = self;
    
    self.activityIndicator.alpha = 0;
    

    self.popupContainer.alpha = 0;
    self.popupContainer.userInteractionEnabled = NO;
    
    self.containerViewForSecurity.hidden = YES;
        
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(applicationDidBecomeActive:)
                                                 name:UIApplicationDidBecomeActiveNotification
                                               object:nil];
    
    self.topbarHeightConstraint.constant = 0;
    
     _requestManager=  [[RequestManager alloc] init];

    self.activityIndicator.alpha = 1;
    NSString *udid = [Functions getDeviceUUID];
    NSString *pushToken = GET_VALUE(@"xSync_pushToken");
    
    [_requestManager checkUser:udid
                    pushToken:pushToken ? pushToken : @""
                   completion:^(NSDictionary * result) {
                       self.activityIndicator.alpha = 0;
                       if ( [[result objectForKey:@"code"] intValue] == 200 ){
                           [[Common sharedInstance] setUserId:
                            [[result objectForKey:@"object"] objectForKey:@"id"]];
                           [[Common sharedInstance ] setUserDic:[result objectForKey:@"object"]];
                           [[NSNotificationCenter defaultCenter] postNotificationName:@"setUserDataToLabel"
                                                    object:nil];
                           [self loadWebsite:[NSString stringWithFormat:
                                    @"%@/user/entrance?udid=%@", kHTDFHome, udid]];
                       }else {
                           [self loadWebsite:
                            [NSString stringWithFormat:@"%@?udid=%@",
                             kHTDFIntro, udid]];
                       }
                   }];
    [self setPushManager];
}

- (void)viewDidAppear:(BOOL)animated {
    
}



-(void)applicationDidBecomeActive:(NSNotification *) noti{
    [[NSURLCache sharedURLCache] removeAllCachedResponses];
    [[NSURLCache sharedURLCache] setDiskCapacity:0];
    [[NSURLCache sharedURLCache] setMemoryCapacity:0];
    // PUSH 들어온게 있으면 실행
    
}



#pragma mark - Socket 정보
-(void)setPushManager{
    EmbeddedAppViewController *vovVC = [self.storyboard instantiateViewControllerWithIdentifier:@"embedded_app"];
    
    PushManager *pushManager = [[PushManager alloc]
                                initWithParentVC:self
                                           vovVC:vovVC];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}



- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
//    NSString *segueName = segue.identifier;
    if ([segue.destinationViewController isKindOfClass:NSClassFromString(@"IntroViewController")]){
        
    }
}


#pragma mark - UIWebView
- (void)loadWebsite:(NSString *)URLString {
    [UIView animateWithDuration:0.3f animations:^{
        self.activityIndicator.alpha = 1;
    }];
    
    self.popupContainer.alpha = 0;
    self.popupContainer.userInteractionEnabled = NO;
    
    URLString = [URLString componentsSeparatedByString:@" "][0] ;
    NSString *escapedUrl =
        [URLString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSURL *url = [NSURL URLWithString:escapedUrl ];
    NSURLRequest *request = [NSURLRequest requestWithURL:url
                                             cachePolicy:NSURLRequestReloadIgnoringCacheData
                                         timeoutInterval:30];
    [self.webView loadRequest:request];
    
}

- (void)webViewDidStartLoad:(UIWebView *)webView {

}


- (BOOL)webView:(UIWebView *)webView
shouldStartLoadWithRequest:(NSURLRequest *)request
 navigationType:(UIWebViewNavigationType)navigationType {
    NSString *currentURL = request.URL.absoluteString;
    
    if( !currentURL ) return YES;
    
    // CurrentUrl urlString 입력하기.
    self.urlString = currentURL;
    

    if([currentURL rangeOfString:@"xsyncbrowser"].location != NSNotFound){
        [[UIApplication sharedApplication] openURL:[request URL]];
        return NO;
    }
    
    if( [currentURL containsString:@"/user/entrance"] ){
        self.topbarHeightConstraint.constant = kHeightOfMenuBar;
        self.labelBarTitle.text = @"입장안내";
        [UIView animateWithDuration:0.3f animations:^{
            [self.view layoutIfNeeded];
        }];
        
        NSString *udid = [Functions getDeviceUUID];
        NSString *pushToken = GET_VALUE(@"xSync_pushToken");
        
        [_requestManager checkUser:udid
                         pushToken:pushToken ? pushToken : @""
                        completion:^(NSDictionary * result) {
                            self.activityIndicator.alpha = 0;
                            if ( [[result objectForKey:@"code"] intValue] == 200 ){
                                [[Common sharedInstance ] setUserDic:[result objectForKey:@"object"]];
                                [[Common sharedInstance] setUserId:
                                 [[result objectForKey:@"object"] objectForKey:@"id"]];
                                [[NSNotificationCenter defaultCenter] postNotificationName:@"setUserDataToLabel"
                                                        object:nil];
                            }
                        }];
    }
    
    return YES;
}

-(void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"connectError"
                      object:nil];
    [UIView animateWithDuration:0.3f animations:^{
        self.activityIndicator.alpha = 0;
    }];
}



- (void)webViewDidFinishLoad:(UIWebView *)webView {
    NSString *currentURL = webView.request.URL.absoluteString;
    currentURL = [currentURL componentsSeparatedByString:@"?"][0];
    [UIView animateWithDuration:0.3f animations:^{
        self.activityIndicator.alpha = 0;
    }];
}



- (IBAction)clickMenuButton:(id)sender {
    [self.viewDeckController openSide:IIViewDeckSideLeft animated:YES];
}

@end
